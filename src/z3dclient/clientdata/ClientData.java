/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3dclient.clientdata;

/**
 *
 * @author mattpautzke
 */
public class ClientData {

    private static int clientId = -1;
    private static long playerId = -1;
    private static int groupId = -1;
    private static long clientPing = -1;
    private static boolean connected = false;

    public static synchronized void setClientId(int id) {
        clientId = id;
    }

    public static synchronized int getClientId() {
        return clientId;
    }

    public static synchronized long getPlayerId() {
        return playerId;
    }

    public static synchronized void setPlayerId(long id) {
        playerId = id;
    }

    public static synchronized int getGroupId() {
        return groupId;
    }

    public static synchronized void setGroupId(int id) {
        groupId = id;
    }

    public static synchronized long getPlayerPing() {
        return clientPing;
    }

    public static synchronized void setPlayerPing(long ping) {
        clientPing = ping;
    }

    public static synchronized boolean isConnected() {
        return connected;
    }

    public static synchronized void setConnected(boolean con) {
        connected = con;
    }

    public static synchronized void reset() {
        clientId = -1;
        playerId = -1;
        groupId = -1;
        clientPing = -1;
        connected = false;
    }
}
