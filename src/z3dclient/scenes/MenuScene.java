/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3dclient.scenes;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.input.ChaseCamera;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import z3dclient.client.ClientApp;

/**
 *
 * @author mattpautzke
 */
public class MenuScene extends AbstractAppState {

    private ClientApp app;
    private BulletAppState bulletState;
    
    //Character menu scene
    private ChaseCamera chaseCam;
    private Node menuScene;
    private Node playerNode;
    
    private LightAndEffects LAE;

    protected AnimChannel torsoChannel;
    protected AnimChannel feetChannel;
    protected AnimControl animControl;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (ClientApp) app;
        initStillBackground();
    }

    private void initStillBackground() {
        bulletState = new BulletAppState();
        bulletState.setThreadingType(BulletAppState.ThreadingType.PARALLEL);
        app.getStateManager().attach(bulletState);
        bulletState.getPhysicsSpace().setAccuracy(1f/60f);
        
          //Show physics wireframes
        //bulletState.getPhysicsSpace().enableDebug(app.getAssetManager());
        
          //Show model wireframes
//        WireProcessor processor = new WireProcessor(app.getAssetManager());
//        app.getViewPort().addProcessor(processor);
        
        LAE = new LightAndEffects();
        app.getStateManager().attach(LAE);
        
        menuScene = (Node) app.getAssetManager().loadModel("Scenes/TestArena/Arena.j3o");
        CollisionShape sceneShape = CollisionShapeFactory.createMeshShape(menuScene);
        RigidBodyControl landscape = new RigidBodyControl(sceneShape, 0);
        menuScene.addControl(landscape);
        menuScene.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        
        playerNode = new Node();
        Node playerModel = (Node) app.getAssetManager().loadModel("Models/defaultCharacter/defaultCharacter.j3o");
        playerNode.attachChild(playerModel);

        Vector3f cloc = menuScene.getChild("Player_00").getWorldTranslation();
        CharacterControl character = new CharacterControl(new CapsuleCollisionShape(1.5f, 6f, 1), 1f);
        character.setFallSpeed(50);
        character.setGravity(10);
        character.setJumpSpeed(30);

        playerNode.addControl(character);
        menuScene.attachChild(playerNode);

        chaseCam = new ChaseCamera(app.getCamera(), playerNode, app.getInputManager());
        chaseCam.setUpVector(Vector3f.UNIT_Y);
        chaseCam.setMaxDistance(30f);
        chaseCam.setMinDistance(15f);
        chaseCam.setLookAtOffset(new Vector3f(0, 0, 0));
        chaseCam.setInvertVerticalAxis(true);
        chaseCam.setDefaultDistance(15f);
        chaseCam.setSmoothMotion(true);
        chaseCam.setEnabled(true);
        app.getInputManager().setCursorVisible(true);

        app.getRootNode().attachChild(menuScene);
        getPhysicsSpace().addAll(menuScene);
        character.setPhysicsLocation(cloc);

        animControl = playerModel.getChild("model").getControl(AnimControl.class);
        torsoChannel = animControl.createChannel();
        feetChannel = animControl.createChannel();
        torsoChannel.setAnim("Idle01");
        feetChannel.setAnim("Idle01");
    }

    private void setupAnimationController() {
        torsoChannel.addBone(animControl.getSkeleton().getBone("Head"));
        torsoChannel.addBone(animControl.getSkeleton().getBone("Humerus.L"));
        torsoChannel.addBone(animControl.getSkeleton().getBone("Humerus.R"));
        torsoChannel.addBone(animControl.getSkeleton().getBone("Ulna.L"));
        torsoChannel.addBone(animControl.getSkeleton().getBone("Ulna.R"));

        feetChannel.addBone(animControl.getSkeleton().getBone("Thigh.L"));
        feetChannel.addBone(animControl.getSkeleton().getBone("Thigh.R"));
        feetChannel.addBone(animControl.getSkeleton().getBone("Calf.L"));
        feetChannel.addBone(animControl.getSkeleton().getBone("Calf.R"));
        feetChannel.addBone(animControl.getSkeleton().getBone("Chest"));
    }
    
    public void resetLAE(){
        if (LAE != null){
            app.getStateManager().detach(LAE);
            LAE= new LightAndEffects();
            app.getStateManager().attach(LAE);
        }
    }

    public void removeScene() {
        chaseCam.setEnabled(false);
        app.getFlyByCamera().setEnabled(false);
        app.getRootNode().detachChild(menuScene);
        app.getStateManager().detach(bulletState);
        app.getStateManager().detach(LAE);
    }
    
    public PhysicsSpace getPhysicsSpace(){
        return bulletState.getPhysicsSpace();
    }

    @Override
    public void stateDetached(AppStateManager stateManager) {
        super.stateDetached(stateManager);
        removeScene();
    }
    
}
