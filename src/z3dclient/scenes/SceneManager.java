/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3dclient.scenes;

import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import z3dclient.client.ClientApp;
import z3dclient.client.InterfaceController;
import z3ddata.gamemessages.StartGameMessage;

/**
 *
 * @author mattpautzke
 */
public class SceneManager extends AbstractAppState {

    private ClientApp app;
    private MenuScene menuScene = null;
    private GameScene gameScene = null;
    
    public SceneManager(SimpleApplication app){
        this.app = (ClientApp) app;
    }

    public void loadMenuScene() {
        unloadAllScenes();
        menuScene = new MenuScene();
        app.getStateManager().attach(menuScene);
    }

    public void loadGameScene() {
        unloadAllScenes();
        gameScene = new GameScene(app);
        app.getStateManager().attach(gameScene);
    }

    public synchronized boolean startGame(StartGameMessage msg) {
        if (msg.levelName.length() > 0) {
            loadGameScene();
            gameScene.loadScene(msg.levelName);
            app.getStateManager().getState(InterfaceController.class).gotoScreen("load_in");
        } else {
            loadMenuScene();
            app.getStateManager().getState(InterfaceController.class).gotoScreen("lobby");
        }
        return true;
    }
    
    public void unloadAllScenes(){
        app.getStateManager().detach(gameScene);
        app.getStateManager().detach(menuScene);
    }

    public GameScene getGameScene() {
        return gameScene;
    }

    public MenuScene getMenuScene() {
        return menuScene;
    }
}
