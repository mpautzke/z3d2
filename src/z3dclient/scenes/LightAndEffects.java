/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3dclient.scenes;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.FXAAFilter;
import com.jme3.post.filters.LightScatteringFilter;
import com.jme3.post.ssao.SSAOFilter;
import com.jme3.shadow.DirectionalLightShadowFilter;
import com.jme3.shadow.EdgeFilteringMode;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import jme3utilities.sky.SkyControl;
import z3dclient.client.ClientApp;
import z3ddata.util.SLSettings;

/**
 *
 * @author mattpautzke
 */
public class LightAndEffects extends AbstractAppState {

    private ClientApp app;
    //Effects and Lighting
    private Vector3f lightDir;
    private AmbientLight ambientLight;
    private DirectionalLight directionalLight;
    private DirectionalLightShadowFilter dlShadowFilter;
    private LightScatteringFilter lightScatterFilter;
    private FXAAFilter fx;
    private SSAOFilter ssaoFilter;
    private FilterPostProcessor fpp;
    private SkyControl skyControl;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (ClientApp) app;
        setUpLight();
        addSky();
    }

    private void setUpLight() {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Client setting up lighting.");
        // We add light so we see the scene
        ambientLight = new AmbientLight();
        ambientLight.setColor(ColorRGBA.White);
        ambientLight.setName("mainAmbientLight");
        directionalLight = new DirectionalLight();
        directionalLight.setColor(ColorRGBA.White);
        directionalLight.setName("mainDirectionalLight");

        fpp = new FilterPostProcessor(app.getAssetManager());

        if (!SLSettings.getDisplaySettings().getUserData("Shadows").equals(0)) {           
            if (SLSettings.getDisplaySettings().getUserData("Shadows").equals(1)) {
                dlShadowFilter = new DirectionalLightShadowFilter(app.getAssetManager(), 512, 2);
                dlShadowFilter.setEdgeFilteringMode(EdgeFilteringMode.Bilinear);
            } else if (SLSettings.getDisplaySettings().getUserData("Shadows").equals(2)) {
                dlShadowFilter = new DirectionalLightShadowFilter(app.getAssetManager(), 1024, 3);
                dlShadowFilter.setEdgeFilteringMode(EdgeFilteringMode.Dither);
            } else if (SLSettings.getDisplaySettings().getUserData("Shadows").equals(3)) {
                dlShadowFilter = new DirectionalLightShadowFilter(app.getAssetManager(), 2048, 4);
                dlShadowFilter.setEdgeFilteringMode(EdgeFilteringMode.PCF4);
            }
            dlShadowFilter.setLight(directionalLight);
            dlShadowFilter.setLambda(0.55f);
            dlShadowFilter.setShadowIntensity(0.55f);
            dlShadowFilter.setShadowCompareMode(com.jme3.shadow.CompareMode.Hardware);
            fpp.addFilter(dlShadowFilter);
        }

        if (!SLSettings.getDisplaySettings().getUserData("SSAO").equals(0)) {
            if (SLSettings.getDisplaySettings().getUserData("SSAO").equals(1)) {
                ssaoFilter = new SSAOFilter(5f, 5f, 1f, .5f);
            } else if (SLSettings.getDisplaySettings().getUserData("SSAO").equals(2)) {
                ssaoFilter = new SSAOFilter(10, 7, .5f, .3f);
            } else if (SLSettings.getDisplaySettings().getUserData("SSAO").equals(3)) {
                ssaoFilter = new SSAOFilter(20f, 9f, 0.1f, 0.5f);
            }
            fpp.addFilter(ssaoFilter);
        }
//        if (!SLSettings.getDisplaySettings().getUserData("Light_Scatter").equals(0)) {
//            lightScatterFilter = new LightScatteringFilter(sc.getUpdater().getDirection());
//            if (SLSettings.getDisplaySettings().getUserData("Light_Scatter").equals(1)) {
//                lightScatterFilter.setLightDensity(.8f);
//                lightScatterFilter.setNbSamples(20);
//            } else if (SLSettings.getDisplaySettings().getUserData("Light_Scatter").equals(2)) {
//                lightScatterFilter.setLightDensity(.9f);
//                lightScatterFilter.setNbSamples(50);
//            } else if (SLSettings.getDisplaySettings().getUserData("Light_Scatter").equals(3)) {
//                lightScatterFilter.setLightDensity(.9f);
//                lightScatterFilter.setNbSamples(70);
//            }
//            fpp.addFilter(lightScatterFilter);
//        }

        if (!SLSettings.getDisplaySettings().getUserData("FXAA").equals(0)) {
            fx = new FXAAFilter();
            if (SLSettings.getDisplaySettings().getUserData("FXAA").equals(1)) {
                fx.setReduceMul(0);
                fx.setSubPixelShift(0);
                fx.setSpanMax(32f);
                fx.setVxOffset(1 / 32f);
            } else if (SLSettings.getDisplaySettings().getUserData("FXAA").equals(2)) {
                fx.setReduceMul(0);
                fx.setSubPixelShift(0);
                fx.setSpanMax(32f);
                fx.setVxOffset(1 / 32f);
            } else if (SLSettings.getDisplaySettings().getUserData("FXAA").equals(3)) {
                fx.setReduceMul(0);
                fx.setSubPixelShift(0);
                fx.setSpanMax(32f);
                fx.setVxOffset(1 / 32f);
            }
            fpp.addFilter(fx);

        }
        
        app.getViewPort().addProcessor(fpp);
        app.getRootNode().addLight(directionalLight);
        app.getRootNode().addLight(ambientLight);
    }
    
    public void addSky(){
        skyControl = new SkyControl(app.getAssetManager(), app.getCamera(), 0.9f, true, true);
        skyControl.getSunAndStars().setHour(6f);
        skyControl.getSunAndStars().setObserverLatitude(37.4046f * FastMath.DEG_TO_RAD);
        skyControl.getSunAndStars().setSolarLongitude(Calendar.JULY, 10);
        skyControl.setCloudiness(1f);
        skyControl.getUpdater().addViewPort(app.getViewPort());
        skyControl.getUpdater().setAmbientLight(ambientLight);
        skyControl.getUpdater().setMainLight(directionalLight);
        app.getRootNode().addControl(skyControl);
        skyControl.setEnabled(true);
        
        if(dlShadowFilter != null){
            skyControl.getUpdater().addShadowFilter(dlShadowFilter);
        }
    }  

    @Override
    public void stateDetached(AppStateManager stateManager) {
        super.stateDetached(stateManager);
        if (fpp != null) {
            app.getViewPort().removeProcessor(fpp);
        }
        if (skyControl != null){
            app.getRootNode().removeControl(skyControl);
        }
        app.getRootNode().removeLight(ambientLight);
        app.getRootNode().removeLight(directionalLight);

    }
}
