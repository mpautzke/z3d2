/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3dclient.scenes;

import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.collision.shapes.MeshCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.collision.CollisionResults;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import java.util.logging.Level;
import java.util.logging.Logger;
import z3dclient.client.ClientApp;
import z3dclient.client.InterfaceController;
import z3dclient.clientdata.ClientData;
import z3dclient.sync.SyncManager;
import z3ddata.collectables.Collectable;
import z3ddata.collectables.Items;
import z3ddata.collectables.Pistol;
import z3ddata.controls.EntityCharacterControl;
import z3ddata.controls.NetworkInputControl;
import z3ddata.controls.UserInputControl;
import z3ddata.projectiles.BulletManager;
import z3ddata.entities.DefaultCharacter;
import z3ddata.entities.Entities;
import z3ddata.entities.Entity;
import z3ddata.gamemessages.InteractCollectableMessage;
import z3ddata.player.Players;

/**
 *
 * @author mattpautzke
 */
public class GameScene extends AbstractAppState {

    private ClientApp app;
    private BulletAppState bulletState;
    private SyncManager syncManager;
    //Landscape
    private Node gameScene;
    private Node aesthetic;
    private RigidBodyControl sceneRigidBody;
    private Node collectables = new Node();
    
    private LightAndEffects LAE;

    public GameScene(ClientApp app) {
        this.app = app;
        this.syncManager = this.app.getStateManager().getState(SyncManager.class);
        
        bulletState = new BulletAppState();
        bulletState.setThreadingType(BulletAppState.ThreadingType.PARALLEL);
        app.getStateManager().attach(bulletState);
        bulletState.getPhysicsSpace().setAccuracy(1f/100f);
        
        BulletManager bulletManager = new BulletManager();
        app.getStateManager().attach(bulletManager);
    }

    public void loadScene(String name) {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Client: Loading level!");
        aesthetic = new Node();
        gameScene = (Node) app.getAssetManager().loadModel(name);
        Geometry geo = (Geometry) gameScene.getChild("ArenaMesh");
        CollisionShape sceneShape = new MeshCollisionShape(geo.getMesh());
        //CollisionShape sceneShape = CollisionShapeFactory.createMeshShape(scene);
        sceneRigidBody = new RigidBodyControl(sceneShape, 0);
        gameScene.addControl(sceneRigidBody);
        gameScene.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);

        app.getRootNode().attachChild(gameScene);
        app.getRootNode().attachChild(aesthetic);
        bulletState.getPhysicsSpace().addAll(gameScene);

//        //Show physics wireframes
//        bulletState.getPhysicsSpace().enableDebug(app.getAssetManager());
//        
//          //Show model wireframes
//        WireProcessor processor = new WireProcessor(app.getAssetManager());
//        app.getViewPort().addProcessor(processor);
        
        LAE = new LightAndEffects();
        app.getStateManager().attach(LAE);

        gameScene.attachChild(collectables);

    }

    public void addEntity(long entityId, Entities ent, Vector3f location, Quaternion rotation) {
        DefaultCharacter character;
        if (ent == Entities.defaultEnt) {
            character = new DefaultCharacter(app);
        } else {
            return;
        }
        character.createEntity(entityId);
        character.getPhysicsCharacter().warp(location);

        syncManager.addEntity(character.getEntityId(), character);
        gameScene.attachChild(character.getEntity());
    }

    public synchronized void addCollectable(long collectableId, Items item, Vector3f location) {
        if (item == Items.pistol) {
            Pistol pistol = new Pistol(app, collectableId);
            syncManager.addCollectable(collectableId, pistol);
            pistol.spawn(location);
            collectables.attachChild(pistol);
        }
    }
    
    public void removeCollectable(long collectableId){
        Collectable collectable = syncManager.getCollectable(collectableId);
        collectable.getRbc().setEnabled(false);
        getPhyicsSpace().remove(collectable.getRbc());
        collectables.detachChild(collectable);
    }

    public void enterEntity(long playerId, long entityId) {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Player {0} entering entity {1}", new Object[]{playerId, entityId});
        long curEntity = Players.getPlayer(playerId).getEntityId();
        int groupId = Players.getPlayer(playerId).getGroupId();
        //reset current entity
        if (curEntity != -1) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Player {0} exiting current entity {1}", new Object[]{playerId, curEntity});
            Entity entity = syncManager.getEntity(curEntity);
            entity.killEntity();
        }
        Players.getPlayer(playerId).setEntityId(entityId);
        //if we entered an entity, configure its controls, id -1 means enter no entity
        if (entityId != -1) {
            Entity entity = syncManager.getEntity(entityId);
            entity.setPlayerId(playerId);
            entity.showDisplayName();
            entity.addControl(new NetworkInputControl(app.getStateManager().getState(InterfaceController.class).getClient()));
            entity.addControl(new EntityCharacterControl());
            if (ClientData.getPlayerId() == playerId) {
                entity.addControl(new UserInputControl());
                entity.view();
            }
        }
    }

    public void pickupCollectable(long entityId) {
        CollisionResults midRes = new CollisionResults();
        Ray rayMiddle = new Ray(app.getCamera().getLocation(), app.getCamera().getDirection());
        rayMiddle.setLimit(20);
        collectables.collideWith(rayMiddle, midRes);
        if (midRes.size() > 0) {
            Geometry geo = midRes.getClosestCollision().getGeometry();
            Node object = geo.getParent();
            Node col = object.getParent();
            if (col instanceof Collectable){
                Collectable collectable = (Collectable) col;
                syncManager.send(new InteractCollectableMessage(collectable.getItemId(), entityId));
            } 
        }
    }
    
    public Node getCollectablesNode(){
        return collectables;
    }
    
    public PhysicsSpace getPhyicsSpace(){
        return bulletState.getPhysicsSpace();
    }
    
    public void unloadScene() {
        app.getStateManager().detach(LAE);
        app.getRootNode().detachChild(gameScene);
        app.getRootNode().detachChild(aesthetic);
        app.getStateManager().detach(bulletState);
    }

    @Override
    public void stateDetached(AppStateManager stateManager) {
        unloadScene();
    }

}
