/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3dclient.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import z3dclient.client.ClientApp;
import z3dclient.client.InterfaceController;

/**
 *
 * @author mattpautzke
 */
public class ServerRunnable implements Runnable {

    private ClientApp app;
    private static Process serverProcess = null;
    private static BufferedReader reader = null;
    private static boolean starting;
    private final Class<? extends Object> clazz;
    private final boolean redirectStream;

    public ServerRunnable(ClientApp app, final Class<? extends Object> clazz, final boolean redirectStream) {
        this.app = app;
        this.clazz = clazz;
        this.redirectStream = redirectStream;
    }

    public void terminate() {
        closeServerProcess();
    }

    public void closeServerProcess() {
        if (serverProcess != null) {
            serverProcess.destroy();
            serverProcess = null;
        }
    }

    public void run() {
        try {
            starting = true;
            Logger.getLogger(ClientApp.class.getName()).log(Level.INFO, "Starting Server from client");
            String separator = System.getProperty("file.separator");
            String classpath = System.getProperty("java.class.path");
            String path = System.getProperty("java.home")
                    + separator + "bin" + separator + "java";
            ProcessBuilder processBuilder =
                    new ProcessBuilder(path, "-cp",
                    classpath,
                    clazz.getCanonicalName());
            processBuilder.redirectErrorStream(redirectStream);
            serverProcess = processBuilder.start();
            //serverProcess.waitFor();
            reader = new BufferedReader(new InputStreamReader(serverProcess.getInputStream()));
            String line = "";
            while (!line.equals("SERVER_READY")) {
                if (reader.ready()) {
                    line = reader.readLine();
                    System.out.println(line);
                }
                if (line.equals("ERROR")) {
                    app.getStateManager().getState(InterfaceController.class).setHJInfo("Failed to start server!");
                    app.getStateManager().getState(InterfaceController.class).setEnableButton(true);
                    starting = false;
                    return;
                }
            }
            Logger.getLogger(ClientApp.class.getName()).log(Level.INFO, "Server started");
            app.getStateManager().getState(InterfaceController.class).connectToServer();
            app.getStateManager().getState(InterfaceController.class).setEnableButton(true);
            reader.close();
            starting = false;
        } catch (IOException ex) {
            Logger.getLogger(ClientApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean isStarting(){
        return starting;
    }
}
