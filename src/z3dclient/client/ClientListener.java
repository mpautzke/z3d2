/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3dclient.client;

import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.network.Client;
import com.jme3.network.ClientStateListener;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.network.NetworkClient;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import z3dclient.clientdata.ClientData;
import z3ddata.util.SLSettings;
import z3ddata.gamemessages.AddPlayerMessage;
import z3ddata.gamemessages.ChatMessage;
import z3ddata.gamemessages.GameMessage;
import z3ddata.gamemessages.HandshakeMessage;
import z3ddata.gamemessages.JoinMessage;
import z3ddata.gamemessages.StartGameMessage;
import z3ddata.player.Players;
import z3ddata.util.Globals;


/**
 *
 * @author mattpautzke
 */
public class ClientListener extends AbstractAppState implements MessageListener, ClientStateListener {

    private ClientApp app;
    private Client client;
    private LinkedList<GameMessage> messageQueue = new LinkedList<GameMessage>();

    ClientListener(ClientApp app, NetworkClient client) {
        this.app = app;
        this.client = client;
        this.client.addClientStateListener(this);
        this.client.addMessageListener(this, HandshakeMessage.class, JoinMessage.class, AddPlayerMessage.class, ChatMessage.class, StartGameMessage.class);
    }

    public void messageReceived(Object source, final Message message) {
        assert (message instanceof GameMessage);
        if (client != null) {
            app.enqueue(new Callable<Void>() {
                public Void call() throws Exception {
                    enqueueMessage((GameMessage) message);
                    return null;
                }
            });
        }
    }

    protected void enqueueMessage(GameMessage message) {
        messageQueue.add(message);
    }

    @Override
    public void update(float tpf) {
        if (client != null) {
            for (Iterator<GameMessage> it = messageQueue.iterator(); it.hasNext();) {
                GameMessage message = it.next();
                doMessage(message);
                it.remove();
            }
        }
    }

    protected void doMessage(GameMessage message) {
        if (message instanceof HandshakeMessage) {
            HandshakeMessage msg = (HandshakeMessage) message;
            Logger.getLogger(ClientListener.class.getName()).log(Level.INFO, "Got handshake message back");
            if (msg.server_version != Globals.SERVER_VERSION) {
                //setStatusText("Protocol mismatch - update client!");
                Logger.getLogger(ClientListener.class.getName()).log(Level.INFO, "Client protocol mismatch, disconnecting");
                app.getStateManager().getState(InterfaceController.class).setHJInfo("Protocol mismatch, disconnecting");
                app.getStateManager().getState(InterfaceController.class).disconnectClient();
                if (app.getStateManager().getState(InterfaceController.class).isHost()) {
                    app.getStateManager().getState(InterfaceController.class).closeServerProcess();
                }

                return;
            }
            ClientData.setClientId(msg.clientId);
            client.send(new JoinMessage((String) SLSettings.getDisplaySettings().getUserData("Name"), ""));
        } else if (message instanceof JoinMessage) {
            JoinMessage msg = (JoinMessage) message;
            ClientData.setPlayerId(msg.playerId);
            ClientData.setGroupId(msg.groupId);
            if (msg.rejected) {
                Logger.getLogger(ClientListener.class.getName()).log(Level.INFO, "REJECTED!!!!");
                app.getStateManager().getState(InterfaceController.class).disconnectClient();
                if (app.getStateManager().getState(InterfaceController.class).isHost()) {
                    app.getStateManager().getState(InterfaceController.class).setHJInfo("Client rejected by server...shuting down server");
                    app.getStateManager().getState(InterfaceController.class).closeServerProcess();
                }
                return;
            }
            Logger.getLogger(ClientListener.class.getName()).log(Level.INFO, "Client: Got login message back, we're in");
            app.getStateManager().getState(InterfaceController.class).setHostOptions();
            app.getStateManager().getState(InterfaceController.class).gotoScreen("lobby");
        } else if (message instanceof AddPlayerMessage) {
            AddPlayerMessage msg = (AddPlayerMessage) message;
            if (!msg.remove) {
                Logger.getLogger(ClientListener.class.getName()).log(Level.INFO, "Client: Adding player {0}-{1}", new Object[]{msg.name, msg.playerId});
                Players.addNewPlayer(msg.playerId, msg.name);
            } else {
                Logger.getLogger(ClientListener.class.getName()).log(Level.INFO, "Client: Removing player {0}", msg.playerId);
                Players.remove(msg.playerId);
            }
            app.getStateManager().getState(InterfaceController.class).refreshPlayerList();
        } else if (message instanceof ChatMessage) {
            ChatMessage msg = (ChatMessage) message;
            Logger.getLogger(ClientListener.class.getName()).log(Level.INFO, "Client: Received new chat message");
            app.getStateManager().getState(InterfaceController.class).receiveMessage(msg);
        } else if (message instanceof StartGameMessage) {
            StartGameMessage msg = (StartGameMessage) message;
            Logger.getLogger(ClientListener.class.getName()).log(Level.INFO, "Client: Received start message");
            app.getStateManager().getState(InterfaceController.class).startGame(msg);
        }
    }

    public void clientConnected(Client c) {
        Logger.getLogger(ClientListener.class.getName()).log(Level.INFO, "Sending handshake message.");
        client.send(new HandshakeMessage(Globals.PROTOCOL_VERSION, Globals.CLIENT_VERSION, -1));
    }

    public void clientDisconnected(Client c, DisconnectInfo info) {
        app.getStateManager().getState(InterfaceController.class).disconnected();
        app.getStateManager().getState(InterfaceController.class).gotoMainMenu();
    }

    @Override
    public void stateDetached(AppStateManager stateManager) {
        this.client.removeClientStateListener(this);
        this.client.removeMessageListener(this, GameMessage.class);
    }
}
