/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3dclient.client;

import z3ddata.util.SLSettings;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.network.NetworkClient;
import com.jme3.system.AppSettings;
import java.awt.DisplayMode;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import tonegod.gui.controls.buttons.Button;
import tonegod.gui.controls.buttons.CheckBox;
import tonegod.gui.controls.extras.ChatBoxExt;
import tonegod.gui.controls.extras.ChatBoxExt.ChatChannel;
import tonegod.gui.controls.lists.ComboBox;
import tonegod.gui.controls.lists.SelectList;
import tonegod.gui.controls.text.Label;
import tonegod.gui.controls.text.TextField;
import tonegod.gui.controls.windows.Panel;
import tonegod.gui.core.Screen;
import z3dclient.clientdata.ClientData;
import z3dclient.scenes.GameScene;
import z3dclient.scenes.SceneManager;
import z3dclient.sync.SyncManager;
import z3dclient.util.ServerRunnable;
import z3ddata.gamemessages.AddCollectableMessage;
import z3ddata.gamemessages.AddEntityMessage;
import z3ddata.gamemessages.CharacterActionMessage;
import z3ddata.gamemessages.CharacterControlMessage;
import z3ddata.gamemessages.ChatMessage;
import z3ddata.gamemessages.EnterEntityMessage;
import z3ddata.gamemessages.LoadInMessage;
import z3ddata.gamemessages.StartGameMessage;
import z3ddata.gamemessages.SyncCollectableMessage;
import z3ddata.gamemessages.SyncEntityMessage;
import z3ddata.player.Player;
import z3ddata.player.Players;
import z3ddata.util.Globals;
import z3ddata.util.ZNetwork;
import z3dserver.server.ServerApp;

/**
 *
 * @author mattpautzke
 */
public class InterfaceController extends AbstractAppState implements ActionListener {

    private ClientApp app;
    private SceneManager sceneManager;
    
    private Screen screen;
    //Menus
    private Panel current;
    private Panel main_menu;
    private Panel options;
    private Panel host_join;
    private Panel lobby;
    private Panel HUD;
    private Panel load_in;
    private Panel ingame_menu;
    //Options
    private List<DisplayMode> dmList = new ArrayList<DisplayMode>();
    //Host variables
    private boolean host;
    //Buttons
    private Button hjBTNJoin;
    private Button hjBTNHost;
    private Button mmBTNPlay;
    private Button mmBTNOptions;
    private Button mmBTNExit;
    private Button lbyBTNStart;
    private Button ldnBTNLoadin;
    private SelectList lbySELPlayers;
    private ChatBoxExt lbyCBEChat;
    private ChatChannel lbyCCgeneral;
    private TextField hjTFIPAddress;
    private BitmapFont guiFont;
    private BitmapText info;
    private BitmapText crossHair;
    private float infoUpdatefl = 0;
    
        //Used for client management
    private static ZNetwork zNetwork;
    private ClientListener clientListener = null;
    //Used for server management
    private static Thread serverThread = null;
    private static ServerRunnable serverRunnable = null;
    private SyncManager syncManager;
    

    private enum screenName {
        main_menu,
        options,
        host_join,
        lobby,
        ingame_menu,
        load_in,
        HUD;
    }
    
    public InterfaceController(SimpleApplication app){
        this.app = (ClientApp) app;
        zNetwork = new ZNetwork();
        initPanels();
        initForm();
    }
        

    private void initPanels() {
        app.getInputManager().deleteMapping(SimpleApplication.INPUT_MAPPING_EXIT);
        app.getInputManager().setCursorVisible(true);
        
        sceneManager = new SceneManager(app);
        app.getStateManager().attach(sceneManager);
        sceneManager.loadMenuScene();
        
        screen = new Screen(app, "Interface/ZDef/style_map.gui.xml");
        screen.parseLayout("Interface/userInterface.gui.xml", this);

        main_menu = (Panel) screen.getElementById("main_menu");
        current = main_menu;

        options = (Panel) screen.getElementById("options");
        options.hide();

        host_join = (Panel) screen.getElementById("host_join");
        host_join.hide();

        lobby = (Panel) screen.getElementById("lobby");
        lobby.hide();

        load_in = (Panel) screen.getElementById("load_in");
        load_in.hide();

        HUD = (Panel) screen.getElementById("HUD");
        HUD.hide();

        ingame_menu = (Panel) screen.getElementById("ingame_menu");
        ingame_menu.hide();


        app.getGuiNode().addControl(screen);

        initKeyboardInput();
        initGuiText();

    }

    private void initForm() {

        // Set up Resolution Drop Down
        List<DisplayMode> temp = new ArrayList<DisplayMode>();
        DisplayMode[] dm = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayModes();
        temp = Arrays.asList(dm);

        for (Iterator<DisplayMode> i = temp.iterator(); i.hasNext();) {
            DisplayMode out = i.next();
            if (!isAInListB(out, dmList)) {
                dmList.add(out);
            }
        }

        ComboBox cb = (ComboBox) screen.getElementById("optCBResolution");
        int count = 0;
        for (Iterator<DisplayMode> i = dmList.iterator(); i.hasNext();) {
            DisplayMode dim = i.next();
            String mode = Integer.toString(dim.getWidth()) + "X" + Integer.toString(dim.getHeight());
            cb.addListItem(mode, count);
            count++;
        }
        bindButtons();
    }

    public void bindButtons() {
        mmBTNPlay = (Button) screen.getElementById("mmBTNPlay");
        mmBTNOptions = (Button) screen.getElementById("mmBTNOptions");
        hjTFIPAddress = (TextField) screen.getElementById("hjTFIPAddress");
        hjBTNJoin = (Button) screen.getElementById("hjBTNJoin");
        hjBTNHost = (Button) screen.getElementById("hjBTNHost");
        ldnBTNLoadin = (Button) screen.getElementById("ldnBTNLoadin");
        lbySELPlayers = (SelectList) screen.getElementById("lbySELPlayer");
        lbyCBEChat = (ChatBoxExt) screen.getElementById("lbyCHTChat");
        lbyCBEChat.addChatChannel("chnl0", "All", "cmdAll", "All", ColorRGBA.Green, true);
        lbyBTNStart = (Button) screen.getElementById("lbyBTNStart");

    }

    public void gotoScreen(final String sscreen) {
        app.enqueue(new Callable() {
            public Object call() throws Exception {
                current.hide();
                screenName sN = screenName.valueOf(sscreen);
                switch (sN) {
                    case main_menu:
                        current = main_menu;
                        break;
                    case options:
                        current = options;
                        break;
                    case host_join:
                        current = host_join;
                        break;
                    case lobby:
                        current = lobby;
                        break;
                    case HUD:
                        current = HUD;
                        break;
                    case load_in:
                        current = load_in;
                        break;
                    case ingame_menu:
                        current = ingame_menu;
                        break;
                }
                current.showWithEffect();
                return null;
            }
        });

    }
    private void initGuiText(){
        guiFont = app.getAssetManager().loadFont("Interface/Fonts/Default.fnt");
        
        info = new BitmapText(guiFont, false);
        info.setSize(guiFont.getCharSet().getRenderedSize());
        info.setLocalTranslation(0, app.getContext().getSettings().getHeight(), 0);
        app.getGuiNode().attachChild(info);
    }
    
    public void setInfo(String message) {
        info.setText(message);
    }
    
    public void showInteractableMessage() {
        CollisionResults midRes = new CollisionResults();
        Ray rayMiddle = new Ray(app.getCamera().getLocation(), app.getCamera().getDirection());
        rayMiddle.setLimit(30);
        app.getStateManager().getState(GameScene.class).getCollectablesNode().collideWith(rayMiddle, midRes);
        String hit = "";
        if (midRes.size() > 0) {
            hit = midRes.getClosestCollision().getGeometry().getName();
        }
        if (hit.length() > 0) {
            setInfo("Press E to pick up " + hit);
        } else {
            setInfo("");
        }
    }

    //Main Menu Button Events
    public void mmPlay(MouseButtonEvent evt, boolean isToggled) {
        setHJInfo("");
        hjTFIPAddress.setText("");
        gotoScreen("host_join");
    }

    public void mmOptions(MouseButtonEvent evt, boolean isToggled) {
        gotoScreen("options");
        setCurrentOptions();
    }

    public void mmExit(MouseButtonEvent evt, boolean isToggled) {
        app.stop();
    }

    //Option button events
    public void optBack(MouseButtonEvent evt, boolean isToggled) {
        gotoScreen("main_menu");
        app.getStateManager().getState(SceneManager.class).getMenuScene().resetLAE();
        if (optionsChanged()) {
            resetWindow();
            resizeUI();
        }

    }

    //Host join button events
    public void hjBack(MouseButtonEvent evt, boolean isToggled) {
        gotoScreen("main_menu");
    }

    public void hjHost(MouseButtonEvent evt, boolean isToggled) {
        setEnableButton(false);
        try {
            startServerProcess(ServerApp.class, true);

        } catch (Exception ex) {
            Logger.getLogger(InterfaceController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void hjJoin(MouseButtonEvent evt, boolean isToggled) {
        TextField tf = (TextField) screen.getElementById("hjTFIPAddress");

        String ip = tf.getText();
        if (ip.length() > 0) {
            setEnableButton(false);
            connectToServer(ip);
        }

    }

    public void lbyLeave(MouseButtonEvent evt, boolean isToggled) {
        disconnectClient();
        if (host == true) {
            closeServerProcess();
            host = false;
        }
        lbyCBEChat.getChatArea().getScrollableArea().removeAllChildren();
    }

    public void lbyStart(MouseButtonEvent evt, boolean isToggled) {
        zNetwork.getClient().send(new StartGameMessage("Scenes/TestArena/Arena.j3o"));
    }

    public void igmEndGame(MouseButtonEvent evt, boolean isToggled) {
        Logger.getLogger(InterfaceController.class.getName()).log(Level.INFO, "EndGame Button Pressed");
        zNetwork.getClient().send(new StartGameMessage(""));
    }

    public void ldnLoadIn(MouseButtonEvent evt, boolean isToggled) {
        zNetwork.getClient().send(new LoadInMessage());
        app.getInputManager().setCursorVisible(false);
        app.getFlyByCamera().setEnabled(false);
        gotoScreen("HUD");
    }

    public void sendMessage(Object command, String msg) {
        zNetwork.getClient().send(new ChatMessage(command, msg));
    }

    public void receiveMessage(ChatMessage msg) {
        lbyCBEChat.receiveMsg(msg.command, msg.message);
    }

    private boolean optionsChanged() {
        boolean changed = false;
        ComboBox cb;
        CheckBox cxb;
        TextField tf;

        tf = (TextField) screen.getElementById("optTFName");
        String oldName = SLSettings.getDisplaySettings().getUserData("Name");
        if (!oldName.equals(tf.getText()) && tf.getText().length() > 0) {
            SLSettings.getDisplaySettings().setUserData("Name", tf.getText());

        }

        cb = (ComboBox) screen.getElementById("optCBFXAA");
        int FXAA = cb.getSelectIndex();
        int oldFXAA = SLSettings.getDisplaySettings().getUserData("FXAA");
        if (oldFXAA != FXAA) {
            SLSettings.getDisplaySettings().setUserData("FXAA", cb.getSelectIndex());

        }

        cb = (ComboBox) screen.getElementById("optCBSSAO");
        int SSAO = cb.getSelectIndex();
        int oldSSAO = SLSettings.getDisplaySettings().getUserData("SSAO");
        if (oldSSAO != SSAO) {
            SLSettings.getDisplaySettings().setUserData("SSAO", cb.getSelectIndex());

        }

        cb = (ComboBox) screen.getElementById("optCBLightScatter");
        int LightScatter = cb.getSelectIndex();
        int oldLightScatter = SLSettings.getDisplaySettings().getUserData("Light_Scatter");
        if (oldLightScatter != LightScatter) {
            SLSettings.getDisplaySettings().setUserData("Light_Scatter", cb.getSelectIndex());

        }

        cb = (ComboBox) screen.getElementById("optCBShadows");
        int Shadows = cb.getSelectIndex();
        int oldShadows = SLSettings.getDisplaySettings().getUserData("Shadows");
        if (oldShadows != Shadows) {
            SLSettings.getDisplaySettings().setUserData("Shadows", cb.getSelectIndex());

        }

        cb = (ComboBox) screen.getElementById("optCBResolution");
        int oldWidth = SLSettings.getDisplaySettings().getUserData("Width");
        int oldHeight = SLSettings.getDisplaySettings().getUserData("Height");
        String resolution = cb.getSelectedListItem().getCaption();
        String delims = "X";
        String[] tokens = resolution.split(delims);
        int Width = Integer.parseInt(tokens[0]);
        int Height = Integer.parseInt(tokens[1]);
        if (oldWidth != Width || oldHeight != Height) {
            SLSettings.getDisplaySettings().setUserData("Width", Width);
            SLSettings.getDisplaySettings().setUserData("Height", Height);
            changed = true;
        }

        cxb = (CheckBox) screen.getElementById("optCXBFullscreen");
        boolean oldFullscreen = SLSettings.getDisplaySettings().getUserData("Fullscreen");
        if (oldFullscreen != cxb.getIsChecked()) {
            SLSettings.getDisplaySettings().setUserData("Fullscreen", cxb.getIsChecked());
            changed = true;
        }

        cxb = (CheckBox) screen.getElementById("optCXBVSync");
        boolean oldVSync = SLSettings.getDisplaySettings().getUserData("VSync");
        if (oldVSync != cxb.getIsChecked()) {
            SLSettings.getDisplaySettings().setUserData("VSync", cxb.getIsChecked());
            changed = true;
        }

        SLSettings.saveDisplaySettings();
        return changed;

    }

    private void setCurrentOptions() {
        ComboBox cb;
        CheckBox cxb;
        TextField tf;

        tf = (TextField) screen.getElementById("optTFName");
        String oldName = SLSettings.getDisplaySettings().getUserData("Name");
        tf.setText(oldName);

        cb = (ComboBox) screen.getElementById("optCBFXAA");
        int oldFXAA = SLSettings.getDisplaySettings().getUserData("FXAA");
        cb.setSelectedIndex(oldFXAA);

        cb = (ComboBox) screen.getElementById("optCBSSAO");
        int oldSSAO = SLSettings.getDisplaySettings().getUserData("SSAO");
        cb.setSelectedIndex(oldSSAO);

        cb = (ComboBox) screen.getElementById("optCBLightScatter");
        int oldLightScatter = SLSettings.getDisplaySettings().getUserData("Light_Scatter");
        cb.setSelectedIndex(oldLightScatter);

        cb = (ComboBox) screen.getElementById("optCBShadows");
        int oldShadows = SLSettings.getDisplaySettings().getUserData("Shadows");
        cb.setSelectedIndex(oldShadows);

        cb = (ComboBox) screen.getElementById("optCBResolution");
        int oldWidth = SLSettings.getDisplaySettings().getUserData("Width");
        int oldHeight = SLSettings.getDisplaySettings().getUserData("Height");
        int index = getIndexFromList(oldWidth, oldHeight);
        cb.setSelectedIndex(index);

        cxb = (CheckBox) screen.getElementById("optCXBFullscreen");
        boolean oldFullscreen = SLSettings.getDisplaySettings().getUserData("Fullscreen");
        cxb.setIsChecked(oldFullscreen);

        cxb = (CheckBox) screen.getElementById("optCXBVSync");
        boolean oldVSYNC = SLSettings.getDisplaySettings().getUserData("VSync");
        cxb.setIsChecked(oldVSYNC);
    }

    private boolean isAInListB(DisplayMode A, List<DisplayMode> B) {
        for (Iterator<DisplayMode> i = B.iterator(); i.hasNext();) {
            DisplayMode in = i.next();
            if (in.getHeight() == A.getHeight() && in.getWidth() == A.getWidth()) {
                return true;
            }

        }
        return false;
    }

    private int getIndexFromList(int width, int height) {
        for (Iterator<DisplayMode> i = dmList.iterator(); i.hasNext();) {
            DisplayMode in = i.next();
            if (in.getHeight() == height && in.getWidth() == width) {
                return dmList.indexOf(in);
            }
        }
        return 0;
    }

    private void resizeUI() {
        int width = SLSettings.getDisplaySettings().getUserData("Width");
        int height = SLSettings.getDisplaySettings().getUserData("Height");
        screen.setGlobalUIScale(width / screen.getWidth(), height / screen.getHeight());

    }

    public void setHJInfo(final String msg) {
        app.enqueue(new Callable() {
            public Object call() throws Exception {
                Label info = (Label) screen.getElementById("hjLblInfo");
                info.setText(msg);
                return null;
            }
        });
    }

    public synchronized void connectToServer() {
        new Thread(new Runnable() {
            public void run() {
                setHJInfo("Connecting");
                if (connectClient(Globals.DEFAULT_SERVER)) {
                    host = true;
                    setHJInfo("Connected");
                }
            }
        }).start();
    }

    public synchronized void connectToServer(final String ip) {
        new Thread(new Runnable() {
            public void run() {
                setHJInfo("Connecting");
                if (connectClient(ip)) {
                    setHJInfo("Connected");
                } else {
                    setHJInfo("Failed to connect to " + ip);
                }
                setEnableButton(true);
            }
        }).start();
    }

    public boolean isHost() {
        return host;
    }

    public void setEnableButton(final Boolean enabled) {
        app.enqueue(new Callable() {
            public Object call() throws Exception {
                hjBTNHost.setIsEnabled(enabled);
                hjBTNJoin.setIsEnabled(enabled);
                mmBTNPlay.setIsEnabled(enabled);
                mmBTNOptions.setIsEnabled(enabled);
                return null;
            }
        });
    }

    public void refreshPlayerList() {
        lbySELPlayers.removeAllListItems();
        for (Iterator<Player> it = Players.getPlayers().iterator(); it.hasNext();) {
            Player player = it.next();
            lbySELPlayers.addListItem(player.getPlayerName(), 100);
        }
    }

    public void setHostOptions() {
        // if (isHost()) {
        lbyBTNStart.show();
        //} else {
        // lbyBTNStart.hide();
        // }

    }
    
    public void startGame(StartGameMessage msg){
        sceneManager.startGame(msg);
    }
    
    public void gotoMainMenu(){
        sceneManager.loadMenuScene();
        gotoScreen("main_menu");
    }

    public void initKeyboardInput() {
        app.getInputManager().addMapping("Escape", new KeyTrigger(KeyInput.KEY_ESCAPE));
        app.getInputManager().addMapping("TAB", new KeyTrigger(KeyInput.KEY_TAB));
        app.getInputManager().addListener(this, "Escape");
        app.getInputManager().addListener(this, "TAB");
    }

    public void onAction(String name, boolean isPressed, float tpf) {
        if (name.equals("Escape") && isPressed) {
            if (current.equals(HUD)) {
                app.getInputManager().setCursorVisible(true);
                app.getFlyByCamera().setEnabled(false);
                gotoScreen("ingame_menu");
            } else if (current.equals(ingame_menu)) {
                app.getInputManager().setCursorVisible(false);
                app.getFlyByCamera().setEnabled(true);
                gotoScreen("HUD");
            }
        }
    }

    @Override
    public void update(float tpf) {
        infoUpdatefl += tpf;
        if (infoUpdatefl > .15){
            if (current.equals(HUD)){
                showInteractableMessage();
            }
            infoUpdatefl = 0;
        }
    }
    
    public void resetWindow() {
        AppSettings appSettings = SLSettings.createAppSettings();
        app.setSettings(appSettings);
        app.restart();
    }
    
    public boolean connectClient(String IP) {
        try {
            if (clientListener != null){
                app.getStateManager().detach(clientListener);
                clientListener = null;
            }
            
            if (syncManager != null){
                app.getStateManager().detach(syncManager);
                syncManager = null;
            }
            Logger.getLogger(ClientApp.class.getName()).log(Level.INFO, "Connect to server");
            zNetwork.createClient(IP, Globals.DEFAULT_PORT_TCP, Globals.DEFAULT_PORT_UDP);
            
            clientListener = new ClientListener(app,zNetwork.getClient());
            app.getStateManager().attach(clientListener);
            
            syncManager = new SyncManager(app,zNetwork.getClient(),AddEntityMessage.class, 
                    EnterEntityMessage.class, CharacterControlMessage.class, CharacterActionMessage.class,
                    AddCollectableMessage.class, SyncCollectableMessage.class, SyncEntityMessage.class);
            app.getStateManager().attach(syncManager);
            
            zNetwork.startClient();
            
            return true;

        } catch (IOException ex) {
            Logger.getLogger(ClientApp.class.getName()).log(Level.SEVERE, null, ex);
            closeServerProcess();
            return false;
        }
    }
        
    public void startServerProcess(final Class<? extends Object> clazz, final boolean redirectStream) {
            if (serverRunnable != null && serverRunnable.isStarting()){
                return;
        }
         closeServerProcess();
         serverRunnable = new ServerRunnable(app, clazz, redirectStream);
         serverThread = new Thread(serverRunnable);
         serverThread.start();
    }
    

    public void closeServerProcess() {
        if (serverRunnable != null) {
            serverRunnable.terminate();
            try {
                serverThread.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(ClientApp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public NetworkClient getClient(){
        return zNetwork.getClient();
    }
    
    public void disconnectClient(){
        zNetwork.disconnectClient();
    }
    
    public void disconnected(){
        app.getStateManager().detach(syncManager);
        app.getStateManager().detach(clientListener);
        Players.removeAll();
        ClientData.reset();
    }
}
