package z3dclient.client;

import com.jme3.app.SimpleApplication;
import com.jme3.system.AppSettings;
import z3ddata.util.MSGSerializer;
import z3ddata.util.SLSettings;

/**
 * test
 *
 * @author mattpautzke
 */
public class ClientApp extends SimpleApplication {
    private static ClientApp clientApp;
    //App states
    private InterfaceController Interface;
    

    public static void main(String[] args) {
        clientApp = new ClientApp();
        AppSettings settings = SLSettings.loadDisplaySettings();
        clientApp.setSettings(settings);
        clientApp.setShowSettings(false);
        clientApp.setPauseOnLostFocus(false);
        clientApp.start();
    }

    @Override
    public void simpleInitApp() {
        MSGSerializer.registerSerializers();
        

        Interface = new InterfaceController(clientApp);
        stateManager.attach(Interface);
    } 

    @Override
    public void destroy() {
        Interface.disconnectClient();
        Interface.closeServerProcess();
        super.destroy();
    }

    @Override
    public void stop() {
        Interface.disconnectClient();
        Interface.closeServerProcess();
        super.stop();
    }
}
