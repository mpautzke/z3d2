/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3dclient.sync;

import com.jme3.app.state.AbstractAppState;
import com.jme3.network.Client;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import z3dclient.client.ClientApp;
import z3dclient.scenes.SceneManager;
import z3ddata.collectables.Collectable;
import z3ddata.controls.NetworkInputControl;
import z3ddata.entities.Entities;
import z3ddata.entities.Entity;
import z3ddata.gamemessages.AddCollectableMessage;
import z3ddata.gamemessages.AddEntityMessage;
import z3ddata.gamemessages.CharacterActionMessage;
import z3ddata.gamemessages.CharacterControlMessage;
import z3ddata.gamemessages.EnterEntityMessage;
import z3ddata.gamemessages.PhysicsSyncMessage;
import z3ddata.gamemessages.SyncCollectableMessage;
import z3ddata.gamemessages.SyncEntityMessage;
import z3ddata.physicsync.SyncMessageValidator;
import z3ddata.util.Globals;

/**
 *
 * @author mattpautzke
 */
public class SyncManager extends AbstractAppState implements MessageListener {

    private ClientApp app;
    private Client client;
    private LinkedList<PhysicsSyncMessage> messageQueue = new LinkedList<PhysicsSyncMessage>();
    private HashMap<Long, Entity> syncEntities = new HashMap<Long, Entity>();
    private HashMap<Long, Collectable> syncCollectables = new HashMap<Long, Collectable>();
    private LinkedList<SyncMessageValidator> validators = new LinkedList<SyncMessageValidator>();
    private double time = 0;
    private double offset = Double.MIN_VALUE;
    private double maxDelay = 0.50;
    private float clientSyncTimer = 0;

    public SyncManager(ClientApp app, Client client, Class... classes) {
        this.app = app;
        this.client = client;
        this.client.addMessageListener(this, classes);
    }

    public void messageReceived(Object source, final Message message) {
        assert (message instanceof PhysicsSyncMessage);
        if (client != null) {
            app.enqueue(new Callable<Void>() {
                public Void call() throws Exception {
                    enqueueMessage((PhysicsSyncMessage) message);
                    return null;
                }
            });
        }
    }

    protected void enqueueMessage(PhysicsSyncMessage message) {
        if (offset == Double.MIN_VALUE) {
            offset = this.time - message.time;
            Logger.getLogger(SyncManager.class.getName()).log(Level.INFO, "Initial offset {0}", offset);
        }
        double delayTime = (message.time + offset) - time;
        if (delayTime > maxDelay) {
            offset -= delayTime - maxDelay;
            Logger.getLogger(SyncManager.class.getName()).log(Level.INFO, "Decrease offset due to high delaytime ({0})", delayTime);
        } else if (delayTime < 0) {
            offset -= delayTime;
            Logger.getLogger(SyncManager.class.getName()).log(Level.INFO, "Increase offset due to low delaytime ({0})", delayTime);
        }
        messageQueue.add(message);
    }

    @Override
    public void update(float tpf) {
        if (client != null) {
            for (Iterator<PhysicsSyncMessage> it = messageQueue.iterator(); it.hasNext();) {
                PhysicsSyncMessage message = it.next();
                doMessage(message);
                it.remove();
            }
            clientSyncTimer += tpf;
            if (clientSyncTimer >= Globals.NETWORK_SYNC_FREQUENCY){
                sendCollectableSync();
                clientSyncTimer = 0;
            }
        }
    }

    protected void doMessage(PhysicsSyncMessage message) {
       if (message instanceof AddEntityMessage) {
           AddEntityMessage msg = (AddEntityMessage) message;
           app.getStateManager().getState(SceneManager.class).getGameScene().addEntity(msg.entityId, Entities.defaultEnt, msg.entityLocation, msg.entityRotation);
        } else if (message instanceof EnterEntityMessage){
           EnterEntityMessage msg = (EnterEntityMessage) message;
           app.getStateManager().getState(SceneManager.class).getGameScene().enterEntity(msg.player_id, msg.entity_id);
        } else if (message instanceof CharacterControlMessage){
            CharacterControlMessage msg = (CharacterControlMessage) message;
            Entity entity = (Entity) syncEntities.get(msg.syncId);
            entity.getControl(NetworkInputControl.class).moveEntity(msg.walkDirection, msg.viewDirection, msg.worldPosition);
        } else if (message instanceof CharacterActionMessage){
            CharacterActionMessage msg = (CharacterActionMessage) message;
            Entity entity = (Entity) syncEntities.get(msg.syncId);
            entity.getControl(NetworkInputControl.class).doAction(msg.action);
        } else if (message instanceof AddCollectableMessage){
            AddCollectableMessage msg = (AddCollectableMessage) message;
            if (msg.entityId == -1){
                app.getStateManager().getState(SceneManager.class).getGameScene().addCollectable(msg.syncId, msg.item, msg.position);
            } else {
                Entity entity = syncEntities.get(msg.entityId);
                Collectable collectable = syncCollectables.get(msg.syncId);
                app.getStateManager().getState(SceneManager.class).getGameScene().removeCollectable(collectable.getItemId());
                entity.addCollectable(collectable);
                syncCollectables.remove(collectable.getItemId());
            }
        } else if (message instanceof SyncCollectableMessage){
            SyncCollectableMessage msg = (SyncCollectableMessage) message;
            Collectable collectable = (Collectable) syncCollectables.get(msg.syncId);
            collectable.getRbc().setPhysicsLocation(msg.location);
            collectable.getRbc().setPhysicsRotation(msg.rotation);
            collectable.getRbc().setLinearVelocity(msg.linearV);
            collectable.getRbc().setAngularVelocity(msg.angularV);
        } else if (message instanceof SyncEntityMessage){
            SyncEntityMessage msg = (SyncEntityMessage) message;
            Entity entity = (Entity) syncEntities.get(msg.syncId);
            entity.getEntity().setLocalTranslation(msg.location);
            entity.getEntity().setLocalRotation(msg.rotation);
        }
    }

    public void addEntity(long id, Entity entity) {
        syncEntities.put(id, entity);
    }
    
    public void addCollectable(long id, Collectable collectable){
        syncCollectables.put(id, collectable);
    }
    
    public void removeCollectable(long id){
        syncCollectables.remove(id);
    }

    public void removeEntity(Entity entity) {
        for (Iterator<Entry<Long, Entity>> it = syncEntities.entrySet().iterator(); it.hasNext();) {
            Entry<Long, Entity> entry = it.next();
            if (entry.getValue() == entity) {
                it.remove();
                return;
            }
        }
    }

    public void removeObject(long id) {
        syncEntities.remove(id);
    }

    public void clearObjects() {
        syncEntities.clear();
    }
    
    public Entity getEntity(long id){
        return syncEntities.get(id);
    }
    
    public Collectable getCollectable(long id){
        return syncCollectables.get(id);
    }

    public void addMessageValidator(SyncMessageValidator validator) {
        validators.add(validator);
    }

    public void removeMessageValidator(SyncMessageValidator validator) {
        validators.remove(validator);
    }
    
    public void send(PhysicsSyncMessage msg) {
        if (client == null) {
            Logger.getLogger(SyncManager.class.getName()).log(Level.SEVERE, "Server: Broadcasting message on client {0}", msg);
            return;
        }
        client.send(msg);
    }
    
    private void sendCollectableSync(){
        for (Iterator<Entry<Long, Collectable>> it = syncCollectables.entrySet().iterator(); it.hasNext();){
            Entry<Long, Collectable> entry = it.next();
            Collectable col = entry.getValue();
            if (col.getRbc().isActive()){
                SyncCollectableMessage msg = new SyncCollectableMessage(entry.getKey(), 
                    col.getRbc().getPhysicsLocation(),col.getRbc().getPhysicsRotation()
                    ,col.getRbc().getLinearVelocity(), col.getRbc().getAngularVelocity());
                send(msg);
            }
        }
    }
}
