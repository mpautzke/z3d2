package z3dserver.server;

import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.network.ConnectionListener;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.network.Server;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import z3ddata.entities.Entities;
import z3ddata.gamemessages.AddPlayerMessage;
import z3ddata.gamemessages.ChatMessage;
import z3ddata.gamemessages.GameMessage;
import z3ddata.gamemessages.HandshakeMessage;
import z3ddata.gamemessages.JoinMessage;
import z3ddata.gamemessages.LoadInMessage;
import z3ddata.gamemessages.StartGameMessage;
import z3ddata.util.Globals;
import z3dserver.clientdata.Clients;

import z3ddata.player.Player;
import z3ddata.player.Players;

/**
 *
 * @author mattpautzke
 */
public class ServerListener extends AbstractAppState implements MessageListener<HostedConnection>, ConnectionListener {

    private ServerApp app;
    private Server server;
    private LinkedList<GameMessage> messageQueue = new LinkedList<GameMessage>();

    public ServerListener(ServerApp app, Server server) {
        this.app = app;
        this.server = server;
        this.server.addConnectionListener(this);
        this.server.addMessageListener(this, HandshakeMessage.class, JoinMessage.class, AddPlayerMessage.class, ChatMessage.class, StartGameMessage.class, LoadInMessage.class);
    }

    public void messageReceived(HostedConnection source, final Message message) {
        assert (message instanceof GameMessage);
        final GameMessage msg = (GameMessage) message;
        msg.clientId = source.getId();
        if (server != null) {
            app.enqueue(new Callable<Void>() {
                public Void call() throws Exception {
                    enqueueMessage((GameMessage) msg);
                    return null;
                }
            });
        }
    }

    protected void enqueueMessage(GameMessage message) {
        messageQueue.add(message);
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        if (server != null) {
            for (Iterator<GameMessage> it = messageQueue.iterator(); it.hasNext();) {
                GameMessage message = it.next();
                doMessage(message);
                it.remove();
            }
        }
    }

    protected void doMessage(GameMessage message) {
        if (message instanceof HandshakeMessage) {
            HandshakeMessage msg = (HandshakeMessage) message;
            Logger.getLogger(ServerListener.class.getName()).log(Level.INFO, "Got handshake message");
            if (msg.protocol_version != Globals.PROTOCOL_VERSION) {
                server.getConnection(msg.clientId).close("Connection Protocol Mismatch - Update Client");
                Logger.getLogger(ServerListener.class.getName()).log(Level.INFO, "Client protocol mismatch, disconnecting");
                return;
            }
            int server_version = Globals.SERVER_VERSION;
            server.getConnection(msg.clientId).send(new HandshakeMessage(-1, -1, server_version));
            Logger.getLogger(ServerListener.class.getName()).log(Level.INFO, "Sent back handshake message");
        } else if (message instanceof JoinMessage) {
            JoinMessage msg = (JoinMessage) message;
            Logger.getLogger(ServerListener.class.getName()).log(Level.INFO, "Got client join message");
            if (!Clients.exsists(msg.clientId)) {
                Logger.getLogger(ServerListener.class.getName()).log(Level.WARNING, "Receiving join message from unknown client");
                return;
            }
            final long newPlayerId = Players.getNew(msg.name);
            Players.getPlayer(newPlayerId).setClientId(msg.clientId);
            //broadcast and add new player joined
            server.broadcast(new AddPlayerMessage(newPlayerId, msg.name));
            Logger.getLogger(ServerListener.class.getName()).log(Level.INFO, "Created new played ID {0}", newPlayerId);
            Clients.setConnected(msg.clientId, true);
            Clients.setPlayerId(msg.clientId, newPlayerId);
            msg.setInfo(newPlayerId, 0, false);
            server.getConnection(msg.clientId).send(msg);
            Logger.getLogger(ServerListener.class.getName()).log(Level.INFO, "Login succesful - sent back join message");
            for (Iterator<Player> it = Players.getPlayers().iterator(); it.hasNext();) {
                Player player = it.next();
                if (player.getPlayerId() != newPlayerId) {
                    //send all current players to the newly joined player
                    server.getConnection(msg.clientId).send(new AddPlayerMessage(player.getPlayerId(), player.getPlayerName()));
                }
            }
        } else if (message instanceof ChatMessage) {
            ChatMessage msg = (ChatMessage) message;
            long playerid = Clients.getPlayerId(msg.clientId);
            msg.message = Players.getPlayer(playerid).getPlayerName() + ":" + msg.message;
            server.broadcast(msg);
        } else if (message instanceof StartGameMessage) {
            Logger.getLogger(ServerListener.class.getName()).log(Level.INFO, "Got a start message.");
            StartGameMessage msg = (StartGameMessage) message;
            app.getGameManager().startGame(msg);
        } else if (message instanceof LoadInMessage){
            LoadInMessage msg = (LoadInMessage) message;
            long playerId = Clients.getPlayerId(msg.clientId);
            long entityId = app.getGameScene().addEntity(Entities.defaultEnt);
            app.getGameScene().enterEntity(playerId, entityId);
        }

    }

    public void connectionAdded(Server server, HostedConnection client) {
        int clientId = (int) client.getId();
        if (!Clients.exsists(clientId)) {
            Clients.add(clientId);
        } else {
            Logger.getLogger(ServerListener.class.getName()).log(Level.SEVERE, "Client ID exists!");
        }
    }

    public void connectionRemoved(Server server, HostedConnection client) {
        int clientId = (int) client.getId();
        long playerId = Clients.getPlayerId(clientId);
        Players.remove(playerId);
        server.broadcast(new AddPlayerMessage(playerId));
        Clients.remove(clientId);
    }

    @Override
    public void stateDetached(AppStateManager stateManager) {
        this.server.removeConnectionListener(this);
        this.server.removeMessageListener(this);
    }
}
