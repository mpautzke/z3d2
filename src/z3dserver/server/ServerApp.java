package z3dserver.server;

import com.jme3.app.SimpleApplication;
import com.jme3.network.Server;
import com.jme3.renderer.RenderManager;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeContext;
import java.util.logging.Level;
import java.util.logging.Logger;
import z3ddata.gamemessages.CharacterActionMessage;
import z3ddata.gamemessages.CharacterControlMessage;
import z3ddata.gamemessages.InteractCollectableMessage;
import z3ddata.gamemessages.SyncCollectableMessage;
import z3ddata.util.Globals;
import z3ddata.util.MSGSerializer;
import z3ddata.util.ZNetwork;
import z3dserver.games.GameManager;
import z3dserver.scenes.GameScene;
import z3dserver.sync.SyncManager;

/**
 * test
 * @author mattpautzke
 */
public class ServerApp extends SimpleApplication {
    private static ServerApp serverApp;
    private static AppSettings serverSettings;
    private static ZNetwork zNetwork;
      
    private ServerListener serverListener;
    private GameScene gameScene;
    private GameManager gameManager;
    private SyncManager syncManager;
    
    public static void main(String[] args) {
        serverSettings = new AppSettings(true);
        serverSettings.setFrameRate(60);
        serverSettings.setRenderer(null);
        serverSettings.setTitle("Server");
        
        serverApp = new ServerApp();
        serverApp.setSettings(serverSettings);
        serverApp.start(JmeContext.Type.Headless);
    }

    @Override
    public void simpleInitApp() {
        zNetwork = new ZNetwork();
        MSGSerializer.registerSerializers();
        try {
            zNetwork.createServer(Globals.DEFAULT_PORT_TCP, Globals.DEFAULT_PORT_UDP);
        } catch (Exception ex) {
            Logger.getLogger(ServerApp.class.getName()).log(Level.SEVERE, "Cannot start server: {0}", ex);
            System.out.println("ERROR");
            System.exit(0);
        }

        serverListener = new ServerListener(this, zNetwork.getServer());
        stateManager.attach(serverListener);

        syncManager = new SyncManager(this, zNetwork.getServer(), CharacterControlMessage.class,
                CharacterActionMessage.class, SyncCollectableMessage.class, InteractCollectableMessage.class);
        stateManager.attach(syncManager);

        gameScene = new GameScene(this);
        stateManager.attach(gameScene);

        gameManager = new GameManager();
        stateManager.attach(gameManager);
        
        System.out.println("SERVER_READY");
        
        ServerInterface serverInterface = new ServerInterface();
        
        System.out.println("AHHHH");
        Logger.getLogger("").log(Level.INFO, "Client protocol mismatch, disconnecting");
    }

    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
    public Server getServer(){
        return zNetwork.getServer();
    }
    
    public GameScene getGameScene(){
        return gameScene;
    }
    
    public GameManager getGameManager(){
        return gameManager;
    }
    
    @Override
    public synchronized void stop() {

        zNetwork.closeServer();
    }
}
