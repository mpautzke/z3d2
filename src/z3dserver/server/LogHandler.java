/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3dserver.server;

import java.util.logging.LogRecord;
import java.util.logging.StreamHandler;
import javax.swing.JTextArea;

/**
 *
 * @author M
 */
public class LogHandler extends StreamHandler{
    private JTextArea textArea;
 
    
    public LogHandler(JTextArea textArea){
        this.textArea = textArea;
    }
 
    @Override
    public void flush() {
        super.flush();
    }
 
 
    @Override
    public void close() throws SecurityException {
        super.close();
    }

    @Override
    public synchronized void publish(LogRecord record) {
        super.publish(record); //To change body of generated methods, choose Tools | Templates.
        textArea.append(record.getLevel() + ":" + record.getMessage() + "\n");
    }
}
