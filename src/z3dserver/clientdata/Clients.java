package z3dserver.clientdata;

import java.util.HashMap;

/**
 * 
 * @author mattpautzke
 */
public class Clients {

    private static HashMap<Integer, ClientData> clients = new HashMap<Integer, ClientData>();


    public static synchronized void add(int id) {
        clients.put(id, new ClientData(id));
    }

    public static synchronized void remove(int id) {
        clients.remove(id);
    }

    public static synchronized boolean exsists(int id) {
        return clients.containsKey(id);
    }

    public static synchronized boolean isConnected(int id) {
        return clients.get(id).isConnected();
    }

    public static synchronized void setConnected(int id, boolean connected) {
        clients.get(id).setConnected(connected);
    }

    public static synchronized long getPlayerId(int id) {
        return clients.get(id).getPlayerId();
    }

    public static synchronized void setPlayerId(int id, long playerId) {
        clients.get(id).setPlayerId(playerId);
    }
    
    public static synchronized void setPlayerPing (int id, long ping){
        clients.get(id).setPlayerPing(ping);
    }
    
    public static synchronized long getPlayerPing (int id){
        return clients.get(id).getPlayerPing();
    }
    
    public static synchronized void removeAllPlayers(){
        clients.clear();
    }

    /**
     * Object implementation of ClientData
     */

}
