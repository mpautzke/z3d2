package z3dserver.clientdata;

/**
 *
 * @author mattpautzke
 */
public class ClientData {
    
    private int id;
    private long playerId;
    private long clientPing;
    private boolean connected;

    public ClientData(int id) {
        this.id = id;
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public long getPlayerPing() {
        return clientPing;
    }

    public void setPlayerPing(long ping) {
        this.clientPing = ping;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
}
