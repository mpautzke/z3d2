package z3dserver.scenes;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import java.util.LinkedList;
import z3ddata.collectables.Items;
import z3dserver.sync.SyncManager;

/**
 *
 * @author mattpautzke
 */
public class CollectableManager extends AbstractAppState{
    
    private SimpleApplication app;
    private SyncManager syncManager;
    private long newCollectableId;
    private boolean spawned = false;
    private long timeout;
    

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (SimpleApplication) app;
        this.syncManager = this.app.getStateManager().getState(SyncManager.class);
    }

    public void spawnCollectable(){
        newCollectableId++;
        LinkedList<Node> sp = app.getStateManager().getState(GameScene.class).getPlayerSpawnPoints();
        app.getStateManager().getState(GameScene.class).addCollectable(newCollectableId, Items.pistol, new Vector3f(10f, 10f, 0f));
    }

    @Override
    public void update(float tpf) {
        if (!spawned){
            spawnCollectable();
            spawned = true;
        }
    }

}
