package z3dserver.scenes;

import com.jme3.app.state.AbstractAppState;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.collision.shapes.MeshCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import z3ddata.collectables.Collectable;
import z3ddata.collectables.Items;
import z3ddata.collectables.Pistol;
import z3ddata.controls.EntityCharacterControl;
import z3ddata.controls.NetworkInputControl;
import z3ddata.entities.DefaultCharacter;
import z3ddata.entities.Entities;
import z3ddata.entities.Entity;
import z3ddata.gamemessages.AddCollectableMessage;
import z3ddata.gamemessages.AddEntityMessage;
import z3ddata.gamemessages.EnterEntityMessage;
import z3ddata.player.Players;
import z3ddata.projectiles.BulletManager;
import z3dserver.server.ServerApp;
import z3dserver.sync.SyncManager;

/**
 *
 * @author mattpautzke
 */
public class GameScene extends AbstractAppState {

    private ServerApp app;
    private SyncManager syncManager;
    private BulletAppState bulletState;
    private CollectableManager collectableManager;
    private Node gameScene;
    private Node collectables = new Node();
    private RigidBodyControl sceneRigidBody;
    private LinkedList<Node> playerSpawnPoints = new LinkedList<Node>();
    private long newEntityId;
    
    public GameScene(ServerApp app){
        this.app = (ServerApp) app;
        this.syncManager = this.app.getStateManager().getState(SyncManager.class);
        
        bulletState = new BulletAppState();
        bulletState.setThreadingType(BulletAppState.ThreadingType.PARALLEL);
        app.getStateManager().attach(bulletState);
        bulletState.getPhysicsSpace().setAccuracy(1f/100f);
        
        BulletManager bulletManager = new BulletManager();
        app.getStateManager().attach(bulletManager);
        bulletManager.setIsServer(true);
    }

    public void loadScene(String name) {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Server: Loading level!");
        gameScene = (Node) app.getAssetManager().loadModel(name);
        List<Spatial> children = gameScene.getChildren();
        for (ListIterator<Spatial> child = children.listIterator(); child.hasNext();) {
            Spatial object = child.next();
            if (object.getName().matches(".*Player.*")) {
                playerSpawnPoints.add((Node)object);
            }
        }
        Geometry geo = (Geometry) gameScene.getChild("ArenaMesh");
        CollisionShape sceneShape = new MeshCollisionShape(geo.getMesh());
        sceneRigidBody = new RigidBodyControl(sceneShape, 0);
        gameScene.addControl(sceneRigidBody);
        app.getRootNode().attachChild(gameScene);
        getPhysicsSpace().addAll(gameScene);
        
        collectableManager = new CollectableManager();
        app.getStateManager().attach(collectableManager);
        
        gameScene.attachChild(collectables);
    }

    public long addEntity(Entities ent) {
        newEntityId++;
        
        DefaultCharacter character = null;
        if (ent == Entities.defaultEnt){
            character = new DefaultCharacter(app);
        }
        character.createEntity(newEntityId);
        character.getPhysicsCharacter().warp(playerSpawnPoints.get(0).getWorldTranslation());

        syncManager.addEntity(character.getEntityId(), character);
        gameScene.attachChild(character.getEntity());
        syncManager.broadcast(new AddEntityMessage(character.getEntityId(), ent, playerSpawnPoints.get(0).getWorldTranslation(), new Quaternion()));
        
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Server: Adding entity: {0}", character.getEntityId());
        return character.getEntityId();
    }
    
    public void addCollectable(long collectableId, Items item, Vector3f location) {
        syncManager.broadcast(new AddCollectableMessage(collectableId, item, location));
        if (item == Items.pistol) {
            Pistol pistol = new Pistol(app, collectableId);
            pistol.spawn(location);
            syncManager.addCollectable(collectableId, pistol);
            collectables.attachChild(pistol);
        }
    }
    
    public void removeCollectable(long collectableId){
        Collectable collectable = syncManager.getCollectable(collectableId);
        collectable.getRbc().setEnabled(false);
        getPhysicsSpace().remove(collectable.getRbc());
        collectables.detachChild(collectable);
    }
    
    public void enterEntity(long playerId, long entityId){
        syncManager.broadcast(new EnterEntityMessage(playerId, entityId)); 
        long curEntity = Players.getPlayer(playerId).getEntityId();
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Player {0} current entiry {1}", new Object[]{playerId, curEntity});
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Player {0} entering entity {1}", new Object[]{playerId, entityId});
        long groupId = Players.getPlayer(playerId).getGroupId();
        if (curEntity != -1) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Server: Player {0} exiting current entity {1}", new Object[]{playerId, curEntity});
            Entity entity = syncManager.getEntity(curEntity);
            entity.killEntity();
        }
        Players.getPlayer(playerId).setEntityId(entityId);

        //if we entered an entity, configure its controls, id -1 means enter no entity
        if (entityId != -1) {
            Entity entity = syncManager.getEntity(entityId);
            entity.setPlayerId(playerId);
            entity.addControl(new NetworkInputControl());
            entity.addControl(new EntityCharacterControl());
        }       
    }
    
    public LinkedList<Node> getPlayerSpawnPoints(){
        return playerSpawnPoints;
    }
    
    public PhysicsSpace getPhysicsSpace(){
        return bulletState.getPhysicsSpace();
    }

    public void unloadScene() {
        app.getRootNode().detachChild(gameScene);
    }
}
