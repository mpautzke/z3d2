package z3dserver.sync;

import com.jme3.app.state.AbstractAppState;
import com.jme3.network.Filters;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.network.Server;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import z3ddata.collectables.Collectable;
import z3ddata.controls.NetworkInputControl;
import z3ddata.entities.Entity;
import z3ddata.gamemessages.AddCollectableMessage;
import z3ddata.gamemessages.CharacterActionMessage;
import z3ddata.gamemessages.CharacterControlMessage;
import z3ddata.gamemessages.InteractCollectableMessage;
import z3ddata.gamemessages.PhysicsSyncMessage;
import z3ddata.gamemessages.SyncCollectableMessage;
import z3ddata.gamemessages.SyncEntityMessage;
import z3ddata.physicsync.SyncMessageValidator;
import z3ddata.util.Globals;
import z3dserver.scenes.GameScene;
import z3dserver.server.ServerApp;

/**
 *
 * @author mattpautzke
 */
public class SyncManager extends AbstractAppState implements MessageListener {

    private ServerApp app;
    private Server server;
    private HashMap<Long, Entity> syncEntities = new HashMap<Long, Entity>();
    private HashMap<Long, Collectable> syncCollectables = new HashMap<Long, Collectable>();
    private LinkedList<PhysicsSyncMessage> messageQueue = new LinkedList<PhysicsSyncMessage>();
    private LinkedList<SyncMessageValidator> validators = new LinkedList<SyncMessageValidator>();
    private double time = 0;
    private double offset = Double.MIN_VALUE;
    private double maxDelay = 0.50;
    private float syncTimer = 0;

    public SyncManager(ServerApp app, Server server, Class... classes) {
        this.app = app;
        this.server = server;
        this.server.addMessageListener(this, classes);
    }

    @Override
    public void update(float tpf) {
        if (server != null) {
            syncTimer += tpf;
            if (syncTimer >= Globals.NETWORK_SYNC_FREQUENCY) {
                //sendEntitySync();
                sendCollectableSync();
                syncTimer = 0;
            }
        }
    }

    public void messageReceived(Object source, final Message message) {
        if (server != null) {
            app.enqueue(new Callable<Void>() {
                public Void call() throws Exception {
                    for (Iterator<SyncMessageValidator> it = validators.iterator(); it.hasNext();) {
                        SyncMessageValidator syncMessageValidator = it.next();
                        if (!syncMessageValidator.checkMessage((PhysicsSyncMessage) message)) {
                            return null;
                        }
                    }

                    PhysicsSyncMessage msg = (PhysicsSyncMessage) message;
                    doMessage((PhysicsSyncMessage) message);
                    return null;
                }
            });
        }
    }

    protected void doMessage(PhysicsSyncMessage message) {
        if (message instanceof CharacterControlMessage){
            CharacterControlMessage msg = (CharacterControlMessage) message;
            Entity entity = syncEntities.get(msg.syncId);
            long playerId = entity.getPlayerId();
            filterBroadcast(playerId, (PhysicsSyncMessage) msg);
            entity.getControl(NetworkInputControl.class).moveEntity(msg.walkDirection, msg.viewDirection, msg.worldPosition);
        } else if(message instanceof CharacterActionMessage){
            CharacterActionMessage msg = (CharacterActionMessage) message;
            Entity entity = syncEntities.get(msg.syncId);
            long playerId = entity.getPlayerId();
            filterBroadcast(playerId, (PhysicsSyncMessage) msg);
            entity.getControl(NetworkInputControl.class).doAction(msg.action);
        } else if (message instanceof SyncCollectableMessage){
            SyncCollectableMessage msg = (SyncCollectableMessage) message;
            Collectable collectable = syncCollectables.get(msg.syncId);
            collectable.getRbc().setPhysicsLocation(msg.location);
            collectable.getRbc().setPhysicsRotation(msg.rotation);
            collectable.getRbc().setLinearVelocity(msg.linearV);
            collectable.getRbc().setAngularVelocity(msg.angularV);
        } else if (message instanceof InteractCollectableMessage){
            InteractCollectableMessage msg = (InteractCollectableMessage) message;
            Collectable collectable = syncCollectables.get(msg.syncId);
            Entity entity = syncEntities.get(msg.playerId);
            broadcast(new AddCollectableMessage(collectable.getItemId(), entity.getEntityId()));
            app.getStateManager().getState(GameScene.class).removeCollectable(msg.syncId);
            entity.addCollectable(collectable);
            syncCollectables.remove(collectable.getItemId());
        }
    }

    public void addEntity(long id, Entity entity) {
        syncEntities.put(id, entity);
    }
    
    public void addCollectable(long id, Collectable collectable){
        syncCollectables.put(id, collectable);
    }
    
    public void removeCollectable(long id){
        syncCollectables.remove(id);
    }

    /**
     * removes an object from the list of objects managed by this sync manager
     *
     * @param object
     */
    public void removeEntity(Entity entity) {
        for (Iterator<Entry<Long, Entity>> it = syncEntities.entrySet().iterator(); it.hasNext();) {
            Entry<Long, Entity> entry = it.next();
            if (entry.getValue() == entity) {
                it.remove();
                return;
            }
        }
    }

    /**
     * removes an object from the list of objects managed by this sync manager
     *
     * @param id
     */
    public void removeEntity(long id) {
        syncEntities.remove(id);
    }

    public void clearEntities() {
        syncEntities.clear();
    }
    
    public Entity getEntity(long id){
        return syncEntities.get(id);
    }
    
    public Collectable getCollectable(long id){
        return syncCollectables.get(id);
    }

    /**
     * use to broadcast physics control messages if server, applies timestamp to
     * PhysicsSyncMessage, call from OpenGL thread!
     *
     * @param msg
     */
    public void broadcast(PhysicsSyncMessage msg) {
        if (server == null) {
            Logger.getLogger(SyncManager.class.getName()).log(Level.SEVERE, "Server: Broadcasting message on client {0}", msg);
            return;
        }
        msg.time = time;
        server.broadcast(msg);
    }

    public void filterBroadcast(long client, PhysicsSyncMessage msg) {
        if (server == null) {
            Logger.getLogger(SyncManager.class.getName()).log(Level.SEVERE, "Server: Broadcasting message on client {0}", msg);
            return;
        }
        msg.time = time;
        server.broadcast(Filters.notEqualTo(server.getConnection((int) client)), msg);
    }

    /**
     * send data to a specific client
     *
     * @param client
     * @param msg
     */
    public void send(int client, PhysicsSyncMessage msg) {
        if (server == null) {
            Logger.getLogger(SyncManager.class.getName()).log(Level.SEVERE, "Server: Broadcasting message on client {0}", msg);
            return;
        }
        send(server.getConnection(client), msg);
    }

    /**
     * send data to a specific client
     *
     * @param client
     * @param msg
     */
    public void send(HostedConnection client, PhysicsSyncMessage msg) {
        msg.time = time;
        if (client == null) {
            Logger.getLogger(SyncManager.class.getName()).log(Level.SEVERE, "Client null when sending: {0}", client);
            return;
        }
        client.send(msg);
    }

    protected void sendEntitySync() {
        for (Iterator<Entry<Long, Entity>> it = syncEntities.entrySet().iterator(); it.hasNext();) {
            Entry<Long, Entity> entry = it.next();
            Entity entity = entry.getValue();
            SyncEntityMessage msg = new SyncEntityMessage(entry.getKey(),entity.getEntity().getLocalTranslation(), 
                entity.getEntity().getLocalRotation(),entity.getPhysicsCharacter().getVelocity());
            broadcast(msg);
        }
    }
    
    private void sendCollectableSync(){
        for (Iterator<Entry<Long, Collectable>> it = syncCollectables.entrySet().iterator(); it.hasNext();){
            Entry<Long, Collectable> entry = it.next();
            Collectable col = entry.getValue();
            if (col.getRbc().isActive()){
                SyncCollectableMessage msg = new SyncCollectableMessage(entry.getKey(), 
                    col.getRbc().getPhysicsLocation(),col.getRbc().getPhysicsRotation()
                    ,col.getRbc().getLinearVelocity(), col.getRbc().getAngularVelocity());
                broadcast(msg);
            }
        }
    }
    
    
}
