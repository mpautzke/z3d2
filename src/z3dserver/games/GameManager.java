package z3dserver.games;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import z3ddata.gamemessages.StartGameMessage;
import z3dserver.server.ServerApp;

/**
 *
 * @author mattpautzke
 */
public class GameManager extends AbstractAppState{
    private ServerApp app;
    private boolean running;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (ServerApp) app;
    }
    
    public synchronized boolean startGame(StartGameMessage msg){
        if (msg.levelName.length() > 0) {
            if (running){
                return false;
            }
            Logger.getLogger(GameManager.class.getName()).log(Level.INFO, "Starting Game");
            app.getGameScene().loadScene(msg.levelName);
            app.getServer().broadcast(msg);
            running = true;
        } else {
            Logger.getLogger(GameManager.class.getName()).log(Level.INFO, "Ending Game");
            app.getGameScene().unloadScene();
            app.getServer().broadcast(msg);
            running = false;
        }
        return true;      
    }  
}
