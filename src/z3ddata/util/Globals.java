package z3ddata.util;

import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.network.NetworkClient;


/**
 * 
 * @author mattpautzke
 */
public class Globals {
    
    public static NetworkClient client;

    public static final String VERSION = "ZombieCubed v1.01";
//    public static final String DEFAULT_SERVER = "192.168.1.24";
    public static final String DEFAULT_SERVER = "127.0.0.1";
//    public static final String DEFAULT_SERVER = "jmonkeyengine.com";
//    public static final String DEFAULT_SERVER = "128.238.56.114";
    public static final int PROTOCOL_VERSION = 1;
    public static final int CLIENT_VERSION = 1;
    public static final int SERVER_VERSION = 1;

    public static final float CHARACTER_SYNC_FREQUENCY = .25f;
    public static final float NETWORK_SYNC_FREQUENCY = .25f;
    public static final float NETWORK_MAX_PHYSICS_DELAY = 0.25f;
    public static final int SCENE_FPS = 60;
    public static final float PHYSICS_FPS = 1f / 60f;
    //only applies for client, server doesnt render anyway
    public static final boolean PHYSICS_THREADED = true;
    public static final boolean PHYSICS_DEBUG = false;
    public static final int DEFAULT_PORT_TCP = 4242;
    public static final int DEFAULT_PORT_UDP = 4242;
    
    public static final String LWJGL_OPENGL1 = "LWJGL-OPENGL1";
    public static final String LWJGL_OPENGL2 = "LWJGL-OpenGL2";
    public static final String LWJGL_OPENGL3 = "LWJGL-OpenGL3";
    public static final String LWJGL_OPENGL_ANY = "LWJGL-OpenGL-Any";
    public static final String LWJGL_OPENAL = "LWJGL";
    public static final String SUN_JAVA_COMMAND = "sun.java.command";
    
    public static Quaternion roll90 = new Quaternion(Quaternion.ZERO.fromAngleAxis( FastMath.PI/2 , new Vector3f(0,0,1))); 
    public static Quaternion yaw90 = new Quaternion(Quaternion.ZERO.fromAngleAxis( FastMath.PI/2 , new Vector3f(0,1,0)));   

}
