package z3ddata.util;

import com.jme3.network.serializing.Serializer;
import z3ddata.gamemessages.AddCollectableMessage;
import z3ddata.gamemessages.AddEntityMessage;
import z3ddata.gamemessages.AddPlayerMessage;
import z3ddata.gamemessages.CharacterActionMessage;
import z3ddata.gamemessages.CharacterControlMessage;
import z3ddata.gamemessages.ChatMessage;
import z3ddata.gamemessages.EnterEntityMessage;
import z3ddata.gamemessages.HandshakeMessage;
import z3ddata.gamemessages.InteractCollectableMessage;
import z3ddata.gamemessages.JoinMessage;
import z3ddata.gamemessages.LoadInMessage;
import z3ddata.gamemessages.StartGameMessage;
import z3ddata.gamemessages.SyncCollectableMessage;
import z3ddata.gamemessages.SyncEntityMessage;

/**
 *
 * @author mattpautzke
 */
public class MSGSerializer {
    public static void registerSerializers() {
        Serializer.registerClass(HandshakeMessage.class);
        Serializer.registerClass(JoinMessage.class);
        Serializer.registerClass(AddPlayerMessage.class);
        Serializer.registerClass(ChatMessage.class);
        Serializer.registerClass(StartGameMessage.class);
        Serializer.registerClass(LoadInMessage.class);
        Serializer.registerClass(AddEntityMessage.class);
        Serializer.registerClass(EnterEntityMessage.class);
        Serializer.registerClass(CharacterControlMessage.class);
        Serializer.registerClass(CharacterActionMessage.class);
        Serializer.registerClass(AddCollectableMessage.class);
        Serializer.registerClass(SyncCollectableMessage.class);
        Serializer.registerClass(SyncEntityMessage.class);
        Serializer.registerClass(InteractCollectableMessage.class);
    }
    
}
