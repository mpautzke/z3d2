package z3ddata.util;

import com.jme3.scene.Node;
import com.jme3.system.AppSettings;

/**
 *
 * @author mattpautzke
 */
public class SLSettings {

    private static Node gameSettings = new Node();
    private static Node displaySettings = new Node();

    public static AppSettings loadDisplaySettings() {
        displaySettings = (Node) SLUtil.load("Documents/Zombie3D", "display_settings");
        if (displaySettings != null) {
            System.out.println("Found display settings");
        } else {
            System.out.println("Creating default display settings");
            Node DS = new Node();
            DS.setName("PlayerNode");
            DS.setUserData("Width", 640);
            DS.setUserData("Height", 480);
            DS.setUserData("BitsPerPixel", 24);
            DS.setUserData("Frequency", 60);
            DS.setUserData("DepthBits", 24);
            DS.setUserData("StencilBits", 0);
            DS.setUserData("Samples", 0);
            DS.setUserData("Fullscreen", false);
            DS.setUserData("Title", "Zombie3D");
            DS.setUserData("Renderer", Globals.LWJGL_OPENGL2);
            DS.setUserData("AudioRenderer", Globals.LWJGL_OPENAL);
            DS.setUserData("DisableJoysticks", true);
            DS.setUserData("UseInput", true);
            DS.setUserData("VSync", false);
            DS.setUserData("FrameRate", 0);
            //Other display settings 
            String name = System.getProperty("user.name").toString();
            if (name.length()>12){
                name = name.substring(0, 12);
            }
            DS.setUserData("Name", name);
            DS.setUserData("Shadows", 0);
            DS.setUserData("Light_Scatter", 0);
            DS.setUserData("SSAO", 0);
            DS.setUserData("FXAA", 0);
            SLUtil.save("Documents/Zombie3D", "display_settings", DS);

            displaySettings = DS;
        }
        return createAppSettings();
    }

    public static AppSettings createAppSettings() {
        AppSettings appSettings = new AppSettings(true);
        int Width = displaySettings.getUserData("Width");
        int Height = displaySettings.getUserData("Height");
        int BitsPerPixel = displaySettings.getUserData("BitsPerPixel");
        int Frequency = displaySettings.getUserData("Frequency");
        int DepthBits = displaySettings.getUserData("DepthBits");
        int StencilBits = displaySettings.getUserData("StencilBits");
        int Samples = displaySettings.getUserData("Samples");
        boolean Fullscreen = displaySettings.getUserData("Fullscreen");
        String Title = displaySettings.getUserData("Title");
        String Renderer = displaySettings.getUserData("Renderer");
        String AudioRenderer = displaySettings.getUserData("AudioRenderer");
        boolean DisableJoysticks = displaySettings.getUserData("DisableJoysticks");
        boolean UseInput = displaySettings.getUserData("UseInput");
        boolean VSync = displaySettings.getUserData("VSync");
        int FrameRate = displaySettings.getUserData("FrameRate");

        appSettings.setWidth(Width);
        appSettings.setHeight(Height);
        appSettings.setBitsPerPixel(BitsPerPixel);
        appSettings.setFrequency(Frequency);
        appSettings.setDepthBits(DepthBits);
        appSettings.setStencilBits(StencilBits);
        appSettings.setSamples(Samples);
        appSettings.setFullscreen(Fullscreen);
        appSettings.setTitle(Title);
        appSettings.setRenderer(Renderer);
        appSettings.setAudioRenderer(AudioRenderer);
        appSettings.setUseJoysticks(DisableJoysticks);
        appSettings.setUseInput(UseInput);
        appSettings.setVSync(VSync);
        appSettings.setFrameRate(FrameRate);
        return appSettings;

    }

    public static void loadGameSettings() {
        gameSettings = (Node) SLUtil.load("Documents/Zombie3D", "game_settings");
        if (gameSettings != null) {
            System.out.println("Found game settings");
        } else {
            System.out.println("Creating default game settings");
            Node PS = new Node();
            PS.setName("DisplaySettings");
            String name = System.getProperty("user.name").toString();
            if (name.length()>12){
                name = name.substring(0, 12);
            }
            PS.setUserData("name", name);
            PS.setUserData("shadows", 0);
            PS.setUserData("light_scattering", 0);
            PS.setUserData("ssao", 0);
            PS.setUserData("fxaa", 0);
            SLUtil.save("Documents/Zombie3D", "game_settings", PS);
            gameSettings = PS;
        }
    }

    public static void saveGameSettings() {
        SLUtil.save("Documents/Zombie3D", "game_settings", SLSettings.getGameSettings());
    }

    public static void saveDisplaySettings() {
        SLUtil.save("Documents/Zombie3D", "display_settings", SLSettings.getDisplaySettings());
    }

    public static Node getGameSettings() {
        return gameSettings;
    }

    public static Node getDisplaySettings() {
        return displaySettings;
    }
}
