package z3ddata.util;

import com.jme3.network.Network;
import com.jme3.network.NetworkClient;
import com.jme3.network.Server;
import java.io.IOException;
import java.net.InetAddress;

/**
 *
 * @author mattpautzke
 */
public class ZNetwork {
    private Server server;
    private NetworkClient client;
    
    public ZNetwork(){
        
    }
    
    public Server getServer(){
        return server;
    }
    
    public NetworkClient getClient(){
        return client;
    }
    
    public void createClient(String IP, int tcpPort, int udpPort) throws IOException{
        client = Network.createClient();
        client.connectToServer(IP, tcpPort, udpPort);
        
    }
    
    public void startClient(){
        client.start();
    }
    
    public boolean disconnectClient() {
        if (client != null && client.isConnected()) {
            client.close();
            client = null;
            
            return true;
        }
        return false;
    }
    
    public void createServer(int tcpPort, int udpPort) throws IOException{
        server = Network.createServer(Globals.DEFAULT_PORT_TCP, Globals.DEFAULT_PORT_UDP);
        server.start();
    }
    
    public void closeServer(){
        if (server.isRunning()) {
            server.close();
        }
    }

    public void setServer(Server svr) {
        server = svr;
    }

    public void setClient(NetworkClient clnt) {
        client = clnt;
    }
 
}
