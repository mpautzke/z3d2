package z3ddata.effects;

import com.jme3.app.SimpleApplication;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;

/**
 *
 * @author mattpautzke
 */
public class SparkEmitter{
    
    protected ParticleEmitter sparkEmitter;
    
    public SparkEmitter(SimpleApplication app){
        Material debris_mat = new Material(app.getAssetManager(),
                "Common/MatDefs/Misc/Particle.j3md");

        debris_mat.setTexture("Texture", app.getAssetManager().loadTexture(
                "Textures/Effects/Debris.png"));

        sparkEmitter = new ParticleEmitter("EmitterBox", ParticleMesh.Type.Triangle, 10);
        sparkEmitter.setMaterial(debris_mat);
        sparkEmitter.setImagesX(3);
        sparkEmitter.setImagesY(3); // 2x2 texture animation
        sparkEmitter.setEndColor(new ColorRGBA(1f, 1f, 0f, 1f));   // red
        sparkEmitter.setStartColor(new ColorRGBA(1f, 1f, 0f, 1f)); // yellow
        sparkEmitter.getParticleInfluencer().setInitialVelocity(new Vector3f(5f, 3f, 5f));
        sparkEmitter.setStartSize(.2f);
        sparkEmitter.setEndSize(0.0f);
        sparkEmitter.setGravity(0, 0, 0);
        sparkEmitter.setLowLife(.01f);
        sparkEmitter.setHighLife(.1f);
        sparkEmitter.setParticlesPerSec(0.0f);
        sparkEmitter.setInWorldSpace(false);
        sparkEmitter.getParticleInfluencer().setVelocityVariation(0.6f);
        sparkEmitter.setParticlesPerSec(0);
    }
    
    public void emmitParticals(){
        sparkEmitter.killAllParticles();
        sparkEmitter.emitAllParticles();
    }
    
    public ParticleEmitter getSpark(){
        return sparkEmitter;
    }
    
}
