package z3ddata.controls;

import com.jme3.app.SimpleApplication;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.CameraNode;
import z3dclient.scenes.GameScene;
import z3ddata.entities.Entity;
import z3ddata.gamemessages.CharacterControlMessage;

/**
 *
 * @author mattpautzke
 */
public class EntityCharacterControl extends EntityControl {

    private Vector3f walkDirection = new Vector3f(Vector3f.ZERO);
    private Vector3f viewDirection = new Vector3f(Vector3f.UNIT_Z);
    private Vector3f directionLeft = new Vector3f(Vector3f.UNIT_X);
    private Quaternion directionQuat = new Quaternion();
    private Vector3f entDir;
    private Vector3f entLeft;
    private boolean left;
    private boolean right;
    private boolean up;
    private boolean down;
    private boolean aim;
    private float mouseX;
    private float mouseY;
    private CameraNode cameraNode;

    public enum Action {
        Jump, Aim, notAim, collectableAction, equipCollectable1, pickupCollectable;
    }

    @Override
    protected void controlUpdate(float tpf) {
        entDir = entity.getEntity().getWorldRotation().mult(Vector3f.UNIT_Z).mult(entity.getSpeed());
        entLeft = entity.getEntity().getWorldRotation().mult(Vector3f.UNIT_X).mult(entity.getSpeed());
        
        if (left) {
            walkDirection.addLocal(entLeft);
        }

        if (right) {
            walkDirection.addLocal(entLeft.negate());
        }

        if (up) {
            walkDirection.addLocal(entDir);
        }

        if (down) {
            walkDirection.addLocal(entDir.negate());
        }

        if (mouseX != 0) {
            directionQuat.fromAngleAxis((FastMath.PI) * tpf * mouseX, Vector3f.UNIT_Y);
            directionQuat.multLocal(walkDirection);
            directionQuat.multLocal(viewDirection);
            directionQuat.multLocal(entLeft);
            mouseX = 0;
        }
        if (mouseY != 0) {
            directionQuat.fromAngleAxis((FastMath.PI) * tpf * mouseY, entLeft);
            directionQuat.multLocal(viewDirection);
            if (viewDirection.getY() > 0.7f || viewDirection.getY() < -0.7f) {
                directionQuat.fromAngleAxis((FastMath.PI) * tpf * -mouseY, entLeft);
                directionQuat.multLocal(viewDirection);
            }
            mouseY = 0;
        }
        Vector3f characterDirection = viewDirection.clone();
        characterDirection.y = 0;
        

        if (entity.getPhysicsCharacter().getWalkDirection() != walkDirection || entity.getPhysicsCharacter().getViewDirection() != viewDirection) {
            
            entity.getPhysicsCharacter().setWalkDirection(walkDirection);
            entity.getPhysicsCharacter().setViewDirection(characterDirection);
            Quaternion direction = new Quaternion();
            Vector3f temp = viewDirection.clone();
            temp.z = 1;
            temp.x = 0;
            direction.lookAt(temp, Vector3f.UNIT_Y); 
            cameraNode.setLocalRotation(direction);
            
            if (entity.getControl(UserInputControl.class) != null) {
                entity.getControl(NetworkInputControl.class).sendMessage(new CharacterControlMessage(walkDirection, viewDirection, entity.getEntity().getWorldTranslation()));
                walkDirection.set(0, 0, 0);
            }
        }
    }

    public void doAction(Action action) {
        switch (action) {
            case Jump:
                entity.getPhysicsCharacter().jump();
                break;
            case Aim:
                setAim(true);
                break;
            case notAim:
                setAim(false);
                break;
            case collectableAction:
                if(entity.itemEquiped()){
                    entity.getEquipedItem().action();
                }
                break;
            case equipCollectable1:
                entity.equipItem(0);
                break;
            case pickupCollectable:
                app.getStateManager().getState(GameScene.class).pickupCollectable(entity.getEntityId());
                break;
        }
    }
    
    @Override
    public void setEntity(SimpleApplication app, Entity entity) {
        super.setEntity(app, entity);
        entity.addControl(new CharacterAnimControl());
        cameraNode = new CameraNode("view_cam", app.getCamera());
        cameraNode.setEnabled(false);
        entity.attachCamera(cameraNode);
    }
    
    public CameraNode getCameraNode(){
        return cameraNode;
    }

    public void setAim(boolean aim) {
        if(!entity.itemEquiped()){
            this.aim = false;
        }else{
            this.aim = aim;
        }
    }

    public boolean isAim() {
        return aim;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public void setUp(boolean up) {
        this.up = up;
    }

    public void setDown(boolean down) {
        this.down = down;
    }

    public void setMouseX(float MouseX) {
        this.mouseX = MouseX;
    }

    public void setMouseY(float MouseY) {
        this.mouseY = MouseY;
    }

    public void setWalkDirection(Vector3f walkDirection) {
        this.walkDirection = walkDirection;
    }

    public void setViewDirection(Vector3f viewDirection) {
        this.viewDirection = viewDirection;
    }

    public void setWorldPosition(Vector3f worldPosition) {
        entity.getPhysicsCharacter().warp(worldPosition);
    }

    public Vector3f getViewDirection() {
        return viewDirection;
    }

    public Vector3f getWalkDirection() {
        return walkDirection;
    }

    public Vector3f getEntDir() {
        return entDir;
    }
}
