package z3ddata.controls;

import com.jme3.app.SimpleApplication;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.scene.control.Control;
import z3ddata.controls.EntityCharacterControl.Action;
import z3ddata.entities.Entity;
import z3ddata.gamemessages.CharacterActionMessage;

/**
 * When attached to a Spatial, searches for ManualControl and sends user input
 * there, only used on client for current user entity.
 *
 * @author mattpautzke
 */
public class UserInputControl extends EntityControl implements Control, ActionListener, AnalogListener {
    //TODO: add support for joysticks, mouse axis etc. and localization

    private float mouseX;
    private float mouseY;

    public UserInputControl() {
    }

    @Override
    public void setEntity(SimpleApplication app, Entity entity) {
        super.setEntity(app, entity);
        prepareInputManager();
    }

    private void prepareInputManager() {
        app.getInputManager().addMapping("Left_Key", new KeyTrigger(KeyInput.KEY_A));
        app.getInputManager().addMapping("Right_Key", new KeyTrigger(KeyInput.KEY_D));
        app.getInputManager().addMapping("Up_Key", new KeyTrigger(KeyInput.KEY_W));
        app.getInputManager().addMapping("Down_Key", new KeyTrigger(KeyInput.KEY_S));
        app.getInputManager().addMapping("Left_Key", new KeyTrigger(KeyInput.KEY_LEFT));
        app.getInputManager().addMapping("Right_Key", new KeyTrigger(KeyInput.KEY_RIGHT));
        app.getInputManager().addMapping("WeaponOne", new KeyTrigger(KeyInput.KEY_1));
        app.getInputManager().addMapping("WeaponTwo", new KeyTrigger(KeyInput.KEY_2));
        app.getInputManager().addMapping("Space_Key", new KeyTrigger(KeyInput.KEY_SPACE));
        app.getInputManager().addMapping("Enter_Key", new KeyTrigger(KeyInput.KEY_RETURN));
        app.getInputManager().addMapping("Pickup_Item", new KeyTrigger(KeyInput.KEY_E));
        app.getInputManager().addMapping("Left_Mouse_Button", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        app.getInputManager().addMapping("Right_Mouse_Button", new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
        app.getInputManager().addMapping("Mouse_Left", new MouseAxisTrigger(MouseInput.AXIS_X, true));
        app.getInputManager().addMapping("Mouse_Right", new MouseAxisTrigger(MouseInput.AXIS_X, false));
        app.getInputManager().addMapping("Mouse_Up", new MouseAxisTrigger(MouseInput.AXIS_Y, true));
        app.getInputManager().addMapping("Mouse_Down", new MouseAxisTrigger(MouseInput.AXIS_Y, false));
        app.getInputManager().addListener(this,
                "Left_Key",
                "Right_Key",
                "Up_Key",
                "Down_Key",
                "Left_Key",
                "Right_Key",
                "WeaponOne",
                "WeaponTwo",
                "Space_Key",
                "Enter_Key",
                "Pickup_Item",
                "Left_Mouse_Button",
                "Right_Mouse_Button",
                "Mouse_Left",
                "Mouse_Right",
                "Mouse_Up",
                "Mouse_Down");
    }

    public void onAnalog(String binding, float value, float tpf) {
        if (binding.equals("Mouse_Left")) {
            mouseX = value / tpf;
            entity.getControl(EntityCharacterControl.class).setMouseX(mouseX);
        } else if (binding.equals("Mouse_Right")) {
            mouseX = value / tpf;
            entity.getControl(EntityCharacterControl.class).setMouseX(-mouseX);
        } else if (binding.equals("Mouse_Up")) {
            mouseY = value / tpf;
            entity.getControl(EntityCharacterControl.class).setMouseY(mouseY);
        } else if (binding.equals("Mouse_Down")) {
            mouseY = value / tpf;
            entity.getControl(EntityCharacterControl.class).setMouseY(-mouseY);
        }
    }

    public void onAction(String binding, boolean value, float tpf) {
        if (binding.equals("Left_Key")) {
            if (value) {
                entity.getControl(EntityCharacterControl.class).setLeft(true);
            } else {
                entity.getControl(EntityCharacterControl.class).setLeft(false);
            }
        } else if (binding.equals("Right_Key")) {
            if (value) {
                entity.getControl(EntityCharacterControl.class).setRight(true);
            } else {
                entity.getControl(EntityCharacterControl.class).setRight(false);
            }
        } else if (binding.equals("Up_Key")) {
            if (value) {
                entity.getControl(EntityCharacterControl.class).setUp(true);
            } else {
                entity.getControl(EntityCharacterControl.class).setUp(false);
            }
        } else if (binding.equals("Down_Key")) {
            if (value) {
                entity.getControl(EntityCharacterControl.class).setDown(true);
            } else {
                entity.getControl(EntityCharacterControl.class).setDown(false);
            }
        } else if (binding.equals("Space_Key")) {
            if (value) {
                entity.getControl(NetworkInputControl.class).sendMessage(new CharacterActionMessage(Action.Jump));
                entity.getControl(EntityCharacterControl.class).doAction(Action.Jump);
            }
        } else if (binding.equals("Enter_Key")) {
        } else if (binding.equals("Pickup_Item")) {
            if(value){
                entity.getControl(EntityCharacterControl.class).doAction(Action.pickupCollectable);
            }
        } else if (binding.equals("Left_Mouse_Button")) {
            if (value) {
                entity.getControl(NetworkInputControl.class).sendMessage(new CharacterActionMessage(Action.collectableAction));
                entity.getControl(EntityCharacterControl.class).doAction(Action.collectableAction);
            }
        } else if (binding.equals("Right_Mouse_Button")) {
            if (value) {
                if (entity.getControl(EntityCharacterControl.class).isAim()) {
                    entity.getControl(NetworkInputControl.class).sendMessage(new CharacterActionMessage(Action.notAim));
                    entity.getControl(EntityCharacterControl.class).doAction(Action.notAim);
                } else {
                    entity.getControl(NetworkInputControl.class).sendMessage(new CharacterActionMessage(Action.Aim));
                    entity.getControl(EntityCharacterControl.class).doAction(Action.Aim);
                }
            }
        } else if (binding.equals("WeaponOne")) {
            if (value){
                entity.getControl(NetworkInputControl.class).sendMessage(new CharacterActionMessage(Action.equipCollectable1));
                entity.getControl(EntityCharacterControl.class).doAction(Action.equipCollectable1);
            }
        } else if (binding.equals("WeaponTwo")) {
        }
    }

    @Override
    protected void controlUpdate(float tpf) {
    }

}
