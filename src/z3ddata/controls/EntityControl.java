package z3ddata.controls;

import com.jme3.app.SimpleApplication;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import z3ddata.entities.Entity;

/**
 *
 * @author mattpautzke
 */
public class EntityControl extends AbstractControl {
    protected SimpleApplication app;
    protected Entity entity;

    public EntityControl() {
    }

    public void setEntity(SimpleApplication app, Entity entity){
        this.app = app;
        this.entity = entity;
    }
    
    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        
    }

    public Control cloneForSpatial(Spatial spatial) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void controlUpdate(float tpf) {
        entity.getEntityModel().updateModelBound();
        
    }
 
}
