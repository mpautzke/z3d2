package z3ddata.controls;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.app.SimpleApplication;
import z3ddata.entities.Entity;

/**
 *
 * @author mattpautzke
 */
public class CharacterAnimControl extends EntityControl {

    protected AnimControl animControl;
    protected AnimChannel torsoChannel;
    protected AnimChannel feetChannel;

    @Override
    public void setEntity(SimpleApplication app, Entity entity) {
        super.setEntity(app, entity);
        setupAnimationController();
    }

    @Override
    protected void controlUpdate(float tpf) {
        float velocity = entity.getPhysicsCharacter().getVelocity().length();
        if (velocity > 5) {
            if (!"Run01".equals(feetChannel.getAnimationName())) {
                feetChannel.setAnim("Run01");
                feetChannel.setSpeed(velocity/10);
            }

        } else {
            if (!"Idle01".equals(feetChannel.getAnimationName())) {
                feetChannel.setAnim("Idle01");
            }
        }

        if (entity.getControl(EntityCharacterControl.class).isAim()) {
            if (!"Aim01".equals(torsoChannel.getAnimationName())) {
                torsoChannel.setAnim("Aim01", .5f);
            }
            torsoChannel.setTime(entity.getControl(EntityCharacterControl.class).getViewDirection().y * 1f + 1f);
        } else {
            if (velocity > 5) {
                if (!"Run01".equals(torsoChannel.getAnimationName())) {
                    torsoChannel.setAnim("Run01");
                    torsoChannel.setSpeed(velocity/10);
                }
            } else {
                if (!"Idle01".equals(torsoChannel.getAnimationName())) {
                    torsoChannel.setAnim("Idle01", .5f);
                }
            }
        }
    }

    private void setupAnimationController() {
        animControl = entity.getEntityModel().getChild("model").getControl(AnimControl.class);

        torsoChannel = animControl.createChannel();
        torsoChannel.addBone(animControl.getSkeleton().getBone("Head"));
        torsoChannel.addBone(animControl.getSkeleton().getBone("Humerus.L"));
        torsoChannel.addBone(animControl.getSkeleton().getBone("Humerus.R"));
        torsoChannel.addBone(animControl.getSkeleton().getBone("Ulna.L"));
        torsoChannel.addBone(animControl.getSkeleton().getBone("Ulna.R"));
        torsoChannel.addBone(animControl.getSkeleton().getBone("Hand.L"));
        torsoChannel.addBone(animControl.getSkeleton().getBone("Hand.R"));

        feetChannel = animControl.createChannel();
        feetChannel.addBone(animControl.getSkeleton().getBone("Femur.L"));
        feetChannel.addBone(animControl.getSkeleton().getBone("Femur.R"));
        feetChannel.addBone(animControl.getSkeleton().getBone("Tibia.L"));
        feetChannel.addBone(animControl.getSkeleton().getBone("Tibia.R"));
        feetChannel.addBone(animControl.getSkeleton().getBone("Foot.L"));
        feetChannel.addBone(animControl.getSkeleton().getBone("Foot.R"));
    }
}
