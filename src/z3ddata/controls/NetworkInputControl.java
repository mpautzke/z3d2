package z3ddata.controls;

import com.jme3.math.Vector3f;
import com.jme3.network.Client;
import z3ddata.controls.EntityCharacterControl.Action;
import z3ddata.gamemessages.PhysicsSyncMessage;

/**
 *
 * @author mattpautzke
 */
public class NetworkInputControl extends EntityControl {

    public Client client;

    public NetworkInputControl() {
    }

    public NetworkInputControl(Client client) {
        this.client = client;
    }

    public void moveEntity(Vector3f walkDirection, Vector3f viewDirection, Vector3f worldPosition) {
        entity.getControl(EntityCharacterControl.class).setWorldPosition(worldPosition);
        entity.getControl(EntityCharacterControl.class).setWalkDirection(walkDirection);
        entity.getControl(EntityCharacterControl.class).setViewDirection(viewDirection);
    }

    public void doAction(Action action) {
        entity.getControl(EntityCharacterControl.class).doAction(action);
    }

    public void sendMessage(PhysicsSyncMessage msg) {
        if (client != null && client.isConnected()) {
            msg.syncId = entity.getEntityId();
            client.send(msg);
        }
    }
}
