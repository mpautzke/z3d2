package z3ddata.physicsync;

import z3ddata.gamemessages.PhysicsSyncMessage;

/**
 *
 * @author mattpautzke
 */
public interface SyncMessageValidator {
    public boolean checkMessage(PhysicsSyncMessage message);
}
