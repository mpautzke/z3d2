package z3ddata.gamemessages;

import com.jme3.network.serializing.Serializable;

/**
 *
 * @author mattpautzke
 */
@Serializable()
public class ChatMessage extends GameMessage{
    public String message;
    public Object command;
    
    public ChatMessage(){
        super(true);
    }
    
    public ChatMessage(String message) {
        super(true);
        this.message = message;
    } 
    
    public ChatMessage(Object command, String message){
        super(true);
        this.command = command;
        this.message = message;
    }
}
