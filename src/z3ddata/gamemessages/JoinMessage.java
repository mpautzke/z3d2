/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3ddata.gamemessages;

import com.jme3.network.serializing.Serializable;

/**
 *
 * @author mattpautzke
 */
@Serializable()
public class JoinMessage extends GameMessage{
    public String name;
    public String password;
    public long playerId;
    public int groupId;
    public boolean rejected;

    public JoinMessage() {
        super(true);
    }
    
    public JoinMessage(String name){
        super(true);
        this.name = name;
    }
    
    public JoinMessage(String name, String password){
        super(true);
        this.name = name;
        this.password = password;
    }

    public void setInfo(long playerId, int group_Id, boolean rejected){
        this.playerId = playerId;
        this.groupId = group_Id;
        this.rejected = rejected;
        
    }

}
