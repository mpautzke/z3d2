/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3ddata.gamemessages;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author mattpautzke
 */
@Serializable()
public class SyncEntityMessage extends PhysicsSyncMessage{
    public Vector3f location;
    public Vector3f velocity;
    public Quaternion rotation;
    
    public SyncEntityMessage(){
        
    }
    
    public SyncEntityMessage(long id, Vector3f location, Quaternion rotation, Vector3f velocity){
        this.syncId = id;
        this.location = location;
        this.rotation = rotation;
        this.velocity = velocity;
    }
}
