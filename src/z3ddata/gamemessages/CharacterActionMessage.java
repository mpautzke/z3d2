package z3ddata.gamemessages;

import com.jme3.network.serializing.Serializable;
import z3ddata.controls.EntityCharacterControl.Action;

/**
 *
 * @author mattpautzke
 */
@Serializable
public class CharacterActionMessage extends PhysicsSyncMessage{
    public Action action;
    
    public CharacterActionMessage(){
        
    }
    
    public CharacterActionMessage(Action action){
        this.action = action;
    }
    
}
