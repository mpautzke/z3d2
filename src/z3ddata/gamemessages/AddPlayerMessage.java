package z3ddata.gamemessages;

import com.jme3.network.serializing.Serializable;

/**
 *
 * @author mattpautzke
 */
@Serializable()
public class AddPlayerMessage extends GameMessage{
    public long playerId;
    public String name;
    public boolean remove = false;
    
    public AddPlayerMessage(){
        super(true);
    }
    
    public AddPlayerMessage(long playerId, String name){
        super(true);
        this.playerId = playerId;
        this.name = name;
    }
    
    public AddPlayerMessage(long playerId) {
        super(true);
        this.playerId = playerId;
        this.remove = true;
    }
    
}
