package z3ddata.gamemessages;

import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author mattpautzke
 */
@Serializable
public class CharacterControlMessage extends PhysicsSyncMessage{
    public Vector3f walkDirection = new Vector3f(Vector3f.ZERO);
    public Vector3f viewDirection = new Vector3f(Vector3f.UNIT_Z);
    public Vector3f worldPosition = new Vector3f(Vector3f.ZERO);
    
    public CharacterControlMessage(){
    }
    
    public CharacterControlMessage(Vector3f walkDirection, Vector3f viewDirection, Vector3f worldPosition){
        this.walkDirection = walkDirection;
        this.viewDirection = viewDirection;
        this.worldPosition = worldPosition;
    }
    
}
