package z3ddata.gamemessages;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;
import z3ddata.entities.Entities;

/**
 *
 * @author mattpautzke
 */
@Serializable()
public class AddEntityMessage extends PhysicsSyncMessage {
    public long entityId;
    public Entities entity;
    public Vector3f entityLocation;
    public Quaternion entityRotation;
    
    public AddEntityMessage(){
    }
    
    public AddEntityMessage(long entityId, Entities entity, Vector3f entityLocation, Quaternion entityRotation){
        this.entityId = entityId;
        this.entity = entity;
        this.entityLocation = entityLocation;
        this.entityRotation = entityRotation;
    }
    
}
