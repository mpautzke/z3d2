/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3ddata.gamemessages;

import com.jme3.network.serializing.Serializable;

/**
 *
 * @author mattpautzke
 */
@Serializable()
public class StartGameMessage extends GameMessage {
    public String levelName;
    public String[] modelNames;
    public String winnerName;
    
    public StartGameMessage() {
        super(true);
    }

    public StartGameMessage(String levelName) {
        super(true);
        this.levelName = levelName;
    }

    public StartGameMessage(String levelName, String[] modelNames) {
        super(true);
        this.levelName = levelName;
        this.modelNames = modelNames;
    }
    
    public StartGameMessage(String levelName, String[] modelNames, String winnerName){
        super(true);
        this.levelName = levelName;
        this.modelNames = modelNames;
        this.winnerName = winnerName;
    }
}
