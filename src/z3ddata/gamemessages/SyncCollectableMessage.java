/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3ddata.gamemessages;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author mattpautzke
 */
@Serializable()
public class SyncCollectableMessage extends PhysicsSyncMessage{
    public Vector3f location;
    public Quaternion rotation;
    public Vector3f linearV;
    public Vector3f angularV;
    
    public SyncCollectableMessage(){
        
    }
    
    public SyncCollectableMessage(long id, Vector3f location, Quaternion rotation, Vector3f linearV, Vector3f angularV){
        this.syncId = id;
        this.location = location;
        this.rotation = rotation;
        this.linearV = linearV;
        this.angularV = angularV;
    }
      
}
