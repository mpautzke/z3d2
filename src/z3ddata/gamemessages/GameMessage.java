/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3ddata.gamemessages;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author mattpautzke
 */
@Serializable()
public abstract class GameMessage extends AbstractMessage{
    public int clientId = -1;
    public double time;
    
    public GameMessage() {
        super(true);
    }
    
    public GameMessage(boolean reliable){
        super(reliable);
    }

    public GameMessage(int id) {
        super(true);
        this.clientId = id;
    }
    
    public GameMessage(int id, boolean reliable) {
        super(true);
        this.clientId = id;
    }
    
}
