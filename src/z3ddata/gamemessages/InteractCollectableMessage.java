/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3ddata.gamemessages;

import com.jme3.network.serializing.Serializable;

/**
 *
 * @author mattpautzke
 */
@Serializable()
public class InteractCollectableMessage extends PhysicsSyncMessage{
    public long playerId;
    
    public InteractCollectableMessage(){
        
    }
    
    public InteractCollectableMessage(long collectableId, long playerId){
        this.syncId = collectableId;
        this.playerId = playerId;
    }
}
