/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3ddata.gamemessages;

import com.jme3.network.serializing.Serializable;

/**
 *
 * @author mattpautzke
 */
@Serializable()
public class EnterEntityMessage extends PhysicsSyncMessage {
    public long player_id;
    public long entity_id;
    
    public EnterEntityMessage() {
    }

    public EnterEntityMessage(long player_id, long entity_id) {
        syncId = -1;
        this.player_id = player_id;
        this.entity_id = entity_id;
    }
    
}
