/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3ddata.gamemessages;

import com.jme3.network.serializing.Serializable;

/**
 *
 * @author mattpautzke
 */
@Serializable()
public class HandshakeMessage extends GameMessage{

    public int protocol_version;
    public int client_version;
    public int server_version;
    
     public HandshakeMessage() {
        super(true);
    }

    public HandshakeMessage(int protocol_version, int client_version, int server_version) {
        super(true);
        this.protocol_version = protocol_version;
        this.client_version = client_version;
        this.server_version = server_version;
    }
    
}
