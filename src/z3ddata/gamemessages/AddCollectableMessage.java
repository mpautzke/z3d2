package z3ddata.gamemessages;

import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;
import z3ddata.collectables.Items;

/**
 *
 * @author mattpautzke
 */
@Serializable()
public class AddCollectableMessage extends PhysicsSyncMessage{
    public long entityId = -1;
    public Items item;
    public Vector3f position = Vector3f.ZERO;

    public AddCollectableMessage() {
    }
    
    public AddCollectableMessage(long syncId, Items item, Vector3f position){
        this.syncId = syncId;
        this.item = item;
        this.position = position;
    }
    
    public AddCollectableMessage(long syncId,long entityId){
        this.syncId = syncId;
        this.entityId = entityId;
    }
  
}
