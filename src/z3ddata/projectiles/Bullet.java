package z3ddata.projectiles;

import com.jme3.app.SimpleApplication;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import z3ddata.entities.Entity;

/**
 *
 * @author mattpautzke
 */
public class Bullet {
    private SimpleApplication app;
    private ParticleEmitter bulletEmitter;
    private boolean dead;
    private int damage;
    private float maxLife = 0f;
    private float crnLife = 0f;
    private Entity owner;
    
    public Bullet(SimpleApplication app){
        this.app = app;
        createBulletEffect();
    }
    
    public final void createBulletEffect(){
        Material debris_mat = new Material(app.getAssetManager(),
                "Common/MatDefs/Misc/Particle.j3md");

        debris_mat.setTexture("Texture", app.getAssetManager().loadTexture(
                "Textures/Effects/DebrisA.png"));
        debris_mat.getAdditionalRenderState().setFaceCullMode(RenderState.FaceCullMode.Off);
        debris_mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Off);

        bulletEmitter = new ParticleEmitter("EmitterBox", ParticleMesh.Type.Point, 1);
        bulletEmitter.setMaterial(debris_mat);
        bulletEmitter.setImagesX(3);
        bulletEmitter.setImagesY(3); // 2x2 texture animation
        bulletEmitter.setEndColor(new ColorRGBA(1f, 1f, 0f, 1f));   // red
        bulletEmitter.setStartColor(new ColorRGBA(1f, 1f, 0f, 1f)); // yellow
        bulletEmitter.getParticleInfluencer().setInitialVelocity(new Vector3f(0f, 0f, 0f));
        bulletEmitter.setGravity(0, 0, 0);
        bulletEmitter.setParticlesPerSec(0.0f);
        bulletEmitter.setInWorldSpace(false);
        bulletEmitter.getParticleInfluencer().setVelocityVariation(0.0f);
    }
    
    public void createBullet(Vector3f position, Vector3f direction, float velocity, float damage, float time){
        maxLife = time;
        bulletEmitter.setLocalTranslation(position);
        bulletEmitter.setHighLife(maxLife);
        bulletEmitter.setLowLife(maxLife);
        bulletEmitter.getParticleInfluencer().setInitialVelocity(direction.normalize().mult(velocity));
        bulletEmitter.emitAllParticles();
        app.getRootNode().attachChild(bulletEmitter);
    }
    
    public void acuTime(float tpf){
        crnLife += tpf;
        if (crnLife >= maxLife && !isDead()){
            bulletEmitter.killAllParticles();
            app.getRootNode().detachChild(bulletEmitter);
        }
    }
    
    public boolean isDead(){
        return dead;
    }
    
}
