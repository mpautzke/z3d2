package z3ddata.projectiles;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author mattpautzke
 */
public class BulletManager extends AbstractAppState{
    private SimpleApplication app;
    private boolean isServer = false;
    private LinkedList<Bullet> bullets = new LinkedList<Bullet>();

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app); //To change body of generated methods, choose Tools | Templates.
        this.app = (SimpleApplication) app;
    }
    
    public void addBullet(long owner_id, Vector3f location, Vector3f direction, float maxDistance, float velocity, float damage){
        CollisionResults colRes = new CollisionResults();
        Ray rayMiddle = new Ray(location.add(direction.mult(2)), direction);
        rayMiddle.setLimit(maxDistance);
        app.getRootNode().collideWith(rayMiddle, colRes);
        
        Bullet bullet = new Bullet(app);
        bullet.createBullet(location, direction, velocity, damage, getTime(colRes, velocity));
        
        bullets.add(bullet);
        if (isServer()) {
            if (colRes.size() > 0) {
                int entityId = getClosestCollision(colRes.getClosestCollision());
                if (entityId > 0) {
                    System.out.println("Collided with:" + entityId);
                }
            }
        }
    }

    @Override
    public void update(float tpf) {
        for (Iterator<Bullet> it = bullets.iterator(); it.hasNext();) {
            Bullet bullet = it.next();
            bullet.acuTime(tpf);
            if (bullet.isDead()) {
                it.remove();
            }
        }
    }
    
    public float getTime(CollisionResults cr, float velocity){
        if(cr.size() > 0){
            return cr.getClosestCollision().getDistance()/velocity;
        }else{
            return 3f;
        }
    }
    
    public void setIsServer(boolean isServer){
        this.isServer = isServer;
    }
    
    public int recursiveGetEntityId(Object obj){
        int id = 0;
        if (obj instanceof Node) {
            Node ob = (Node) obj;
            System.out.println(obj.toString());
            String name = ob.getName();
            if (name.matches(".*entity.*")){
                System.out.println("Hit Entity");
                id = Integer.parseInt(name.replaceAll("[^0-9]+", ""));
            } else { 
                id = recursiveGetEntityId(ob.getParent());
            }
        } else {
            System.out.println("End of Ancestory");
        }
        return id;
    }
    
    public int getClosestCollision(CollisionResult cres){
        Geometry geo = cres.getGeometry();
        Object obj = geo.getParent();
        return recursiveGetEntityId(obj);
    }
    
    public boolean isServer(){
        return isServer;
    }
}
