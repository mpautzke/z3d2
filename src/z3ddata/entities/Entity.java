package z3ddata.entities;

import com.jme3.animation.SkeletonControl;
import com.jme3.app.SimpleApplication;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Node;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import z3ddata.collectables.Collectable;
import z3ddata.controls.EntityCharacterControl;
import z3ddata.controls.EntityControl;
import z3ddata.util.Globals;
import z3dserver.sync.SyncManager;

/**
 *
 * @author mattpautzke
 */
public abstract class Entity {

    protected SimpleApplication app;
    protected long entityId = -1;
    protected long playerId = -1;
    protected int health = 100;
    protected boolean alive = true;
    protected String timeOfDeath;
    protected float speed = 30f;
    protected Node entity;
    protected Node entityModel;
    protected Node nameNode;
    protected Node rightHand;
    protected Node leftHand;
    public Node root;
    protected BetterCharacterControl physicsCharacter;
    protected List<EntityControl> controls = new ArrayList<EntityControl>();
    protected List<Collectable> collectables = new ArrayList<Collectable>();
    

    public Entity(SimpleApplication app) {
        this.app = app;
    }

    public long getEntityId() {
        return entityId;
    }

    public Node getEntity() {
        return entity;
    }

    public Node getEntityModel() {
        return entityModel;
    }
    
    public Node getAttachmentNode(String node){
       return entityModel.getChild("model").getControl(SkeletonControl.class).getAttachmentsNode(node);
    }

    public int getHealth() {
        return health;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setEntityId(long id) {
        this.entityId = id;
    }

    public long getPlayerId() {
        return playerId;
    }

    public String getTimeOfDeath() {
        return timeOfDeath;
    }

    public float getSpeed() {
        return speed;
    }
    
    public SimpleApplication getApp(){
        return app;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setTimeOfDeath(String timeOfDeath) {
        this.timeOfDeath = timeOfDeath;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void killEntity() {
        entityId = -1;
        playerId = -1;
        health = 100;
        alive = true;
        timeOfDeath = "now";
        speed = 5f;
    }
    
    public BetterCharacterControl getPhysicsCharacter(){
        return physicsCharacter;
    }
    
    public void addControl(EntityControl control){
        controls.add(control);
        control.setEntity(app, this);
        entityModel.addControl(control);
    }
    
    public <T extends EntityControl> T getControl(Class<T> controlType) {
        for (EntityControl c : controls) {
            if (controlType.isAssignableFrom(c.getClass())) {
                return (T) c;
            }
        }
        return null;
    }
    
    public void addCollectable(Collectable collectable){
        collectable.reset();
        collectable.rotate(Globals.roll90.mult(Globals.yaw90));
        collectable.move(new Vector3f(-.2f,0f,-0.2f));
        collectable.setOwner(playerId); 
        collectables.add(collectable);
        Logger.getLogger(SyncManager.class.getName()).log(Level.SEVERE, "Entity - Location: {0} Rotation: {1}", new Object[]{entity.getWorldTranslation(), entity.getWorldRotation()});
    }
    
    public Collectable getCollectable(int index){
        return collectables.get(index);
    }
    
    public void equipItem(int key){
        if (collectables.size() <= 0){
            return;
        }
        Logger.getLogger(SyncManager.class.getName()).log(Level.SEVERE, "Entity - location: {0} Rotation: {1}", new Object[]{entity.getWorldTranslation(), entity.getWorldRotation()});
        Collectable item = getCollectable(key);
        Logger.getLogger(SyncManager.class.getName()).log(Level.SEVERE, "Collectable - location: {0} Rotation: {1}", new Object[]{item.getWorldTranslation(), item.getWorldRotation()});
        if (item != null){
            rightHand.attachChild(item);
        }
    }
    
    public Collectable getEquipedItem(){
        
        return (Collectable) rightHand.getChild(0);
    }
    
    public boolean itemEquiped(){
        return (rightHand.getChildren().size() > 0);
    }
    
    public void attachCamera(CameraNode camera){
        root.attachChild(camera);
        camera.move(0,3.5f,0);
    }
    
    public void view(){
        getControl(EntityCharacterControl.class).getCameraNode().setEnabled(true);
        
    }

    public abstract void createEntity(long id);
    
    public abstract void showDisplayName();
    
    public abstract void hideDisplayName();
}
