package z3ddata.entities;

import com.jme3.animation.SkeletonControl;
import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.font.Rectangle;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.control.BillboardControl;
import z3ddata.player.Players;

/**
 *
 * @author mattpautzke
 */
public class DefaultCharacter extends Entity {

    public DefaultCharacter(SimpleApplication app) {
        super(app);
    }

    public void createEntity(long id) {
        entityId = id;
        entity = new Node("entity_" + entityId);
        entityModel = (Node) app.getAssetManager().loadModel("Models/defaultCharacter/defaultCharacter.j3o");
        entity.attachChild(entityModel);

        physicsCharacter = new BetterCharacterControl(1.5f, 8f, 1f);
        physicsCharacter.setJumpForce(new Vector3f(0, 30f, 0));

        entity.addControl(physicsCharacter);
        entityModel.move(new Vector3f(0f, 4f, 0));
        rightHand = entityModel.getChild("model").getControl(SkeletonControl.class).getAttachmentsNode("Palm.R");
        leftHand = entityModel.getChild("model").getControl(SkeletonControl.class).getAttachmentsNode("Palm.L");
        root = entityModel.getChild("model").getControl(SkeletonControl.class).getAttachmentsNode("Root");
        app.getStateManager().getState(BulletAppState.class).getPhysicsSpace().add(entity);
        physicsCharacter.setGravity(new Vector3f(0, -50f, 0));
    }

    @Override
    public void showDisplayName() {
        BitmapFont fnt = app.getAssetManager().loadFont("Interface/Fonts/Default.fnt");
        BitmapText txt = new BitmapText(fnt, false);
        txt.setBox(new Rectangle(-3f, 0, 6, 1));
        txt.setAlignment(BitmapFont.Align.Center);
        txt.setQueueBucket(RenderQueue.Bucket.Transparent);
        txt.setSize(1f);
        txt.setText(Players.getPlayer(playerId).getPlayerName());
        txt.setColor(new ColorRGBA(0, 1, 0, .5f));
        nameNode = new Node("NameBillboard");
        BillboardControl nameBillboard = new BillboardControl();
        nameNode.addControl(nameBillboard);
        nameNode.attachChild(txt);
        nameNode.setShadowMode(RenderQueue.ShadowMode.Off);

        entityModel.attachChild(nameNode);
        nameNode.move(0, 8, 0);
    }

    @Override
    public void hideDisplayName() {
        entityModel.detachChild(nameNode);
    }
}
