package z3ddata.player;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

/**
 *
 * @author mattpautzke
 */
public class Players {

    private static HashMap<Long, Player> players = new HashMap<Long, Player>();

    public static synchronized List<Player> getHumanPlayers() {
        LinkedList<Player> list = new LinkedList<Player>();
        for (Iterator<Entry<Long, Player>> it = players.entrySet().iterator(); it.hasNext();) {
            Entry<Long, Player> entry = it.next();
            if (entry.getValue().isHuman()) {
                list.add(entry.getValue());
            }
        }
        return list;
    }
    
    public static synchronized List<Player> getAIPlayers() {
        LinkedList<Player> list = new LinkedList<Player>();
        for (Iterator<Entry<Long, Player>> it = players.entrySet().iterator(); it.hasNext();) {
            Entry<Long, Player> entry = it.next();
            if (!entry.getValue().isHuman()) {
                list.add(entry.getValue());
            }
        }
        return list;
    }
    
     public static synchronized List<Player> getPlayers() {
        LinkedList<Player> list = new LinkedList<Player>(players.values());
        return list;
    }
     
      public static synchronized Player getPlayer(long id) {
        return players.get(id);
    }

    public static synchronized long getNew(String name) {
        long id = 0;
        while (players.containsKey(id)) {
            id++;
        }
        players.put(id, new Player(id, name));
        return id;
    } 

    public static synchronized void add(long id, Player player) {
        players.put(id, player);
    }
    
    public static synchronized void addNewPlayer(long id, String name){
        players.put(id, new Player(id, name));
    }

    public static synchronized void remove(long id) {
        players.remove(id);
    }
    
    public static synchronized void removeAll(){
        players.clear();
    }

    public static synchronized int getAiControl(long id) {
        return players.get(id).getAIControl();
    }

    public static synchronized void setAiControl(long id, int aiControl) {
        players.get(id).setAIControl(aiControl);
    }

    public static synchronized boolean isHuman(long id) {
        return players.get(id).isHuman();
    }
    
}
