package z3ddata.player;

/**
 *
 * @author mattpautzke
 */
public class Player {
    private String name;
    private int clientId;
    private long playerId;
    private long entityId = -1;
    private int groupId = -1;
    private int aiControl = -1;
    
    public Player(long playerId, String name){
        this.playerId = playerId;
        this.name = name;
    }
    
    public boolean isHuman(){
        return aiControl == -1;
    }
    
    public int getAIControl(){
        return aiControl;
    }
    
    public void setAIControl(int aiControl){
        this.aiControl = aiControl;
    }
    
    public void setClientId(int clientId){
        this.clientId = clientId;
    }
    
    public long getPlayerId(){
        return playerId;
    }
    
    public String getPlayerName(){
        return name;
    }
    
    public void setEntityId(long entityId){
        this.entityId = entityId;
    }
    
    public long getEntityId(){
        return entityId;
    }
    
    public void setGroupId(int groupId){
        this.groupId = groupId;
    }
    
    public int getGroupId(){
        return groupId;
    }
    
}
