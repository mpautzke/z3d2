/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3ddata.collectables;

import com.jme3.app.SimpleApplication;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import z3ddata.projectiles.BulletManager;
import z3ddata.effects.SparkEmitter;

/**
 *
 * @author mattpautzke
 */
public class Pistol extends Weapon {
    float maxDistance = 1000f;
    float bulletVelocity= 1500f;
    SparkEmitter gunEffect;

    public Pistol(SimpleApplication app, long id) {
        super(app, Items.pistol, id);
        model = (Node) app.getAssetManager().loadModel("Models/Pistol/Pistol.j3o");
        modelGeo = (Geometry) model.getChild("Pistol");
        gunEffect = new SparkEmitter(app);
        model.attachChild(gunEffect.getSpark());
        gunEffect.getSpark().move(0f,0.1f, 0.7f);
        setDamage(5);
        createRigidBody();
    }

    @Override
    public void shoot() {
        //Emit gun particles 
        //Logger.getLogger(Pistol.class.getName()).log(Level.INFO, "Pistol - location: {0} rotation: {1}", new Object[]{this.getWorldTranslation(), this.getWorldRotation()});
        gunEffect.emmitParticals();
        Vector3f location = model.getWorldTranslation();
        Vector3f direction = model.getWorldRotation().mult(Vector3f.UNIT_Z).normalize();
        app.getStateManager().getState(BulletManager.class).addBullet(owner_id, location, direction, maxDistance, bulletVelocity, damage);
    }
}
