/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3ddata.collectables;

import com.jme3.app.SimpleApplication;

/**
 *
 * @author mattpautzke
 */
public abstract class Weapon extends Collectable{
    protected int damage = 10;
    protected int health = 100;
    protected int ammo = 50;
    protected int clipSize = 10;
    protected int maxAmmo = 200;
    protected int rateOfFire = 1;
    
    
    public Weapon(SimpleApplication app, Items collectable, long id){
       super(app, collectable, id);
    }

    public int getDamage() {
        return damage;
    }

    public int getHealth() {
        return health;
    }

    public int getAmmo() {
        return ammo;
    }

    public int getClipSize() {
        return clipSize;
    }

    public int getMaxAmmo() {
        return maxAmmo;
    }

    public int getRateOfFire() {
        return rateOfFire;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void setAmmo(int ammo) {
        this.ammo = ammo;
    }

    public void setClipSize(int clipSize) {
        this.clipSize = clipSize;
    }

    public void setMaxAmmo(int maxAmmo) {
        this.maxAmmo = maxAmmo;
    }

    public void setRateOfFire(int rateOfFire) {
        this.rateOfFire = rateOfFire;
    }
    
    public abstract void shoot();

    public void action(){
        shoot();
    }
}
