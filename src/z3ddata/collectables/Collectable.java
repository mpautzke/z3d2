/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package z3ddata.collectables;

import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.HullCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 *
 * @author mattpautzke
 */
public abstract class Collectable extends Node {

    protected SimpleApplication app;
    protected Items collectable;
    protected long owner_id;
    protected RigidBodyControl rbc;
    protected Node model;
    protected Geometry modelGeo;
    protected long collectableId;

    public Collectable(SimpleApplication app, Items collectable, long id) {
        this.app = app;
        this.collectable = collectable;
        this.collectableId = id;
    }

    public Items getItemType() {
        return collectable;
    }

    public long getItemId() {
        return collectableId;
    }

    public RigidBodyControl getRbc() {
        return rbc;
    }

    protected void createRigidBody() {
        this.attachChild(model);
        HullCollisionShape shp = new HullCollisionShape(modelGeo.getMesh());
        rbc = new RigidBodyControl(shp, 1);
        this.addControl(rbc);
        
        rbc.setFriction(15f);
        rbc.setCcdMotionThreshold(0.0001f);
        rbc.setCcdSweptSphereRadius(2f);
        rbc.setGravity(new Vector3f(0, -9.8f, 0));
        app.getStateManager().getState(BulletAppState.class).getPhysicsSpace().addAll(this);
    }

    public void spawn(Vector3f position) {
        rbc.setPhysicsLocation(position);
    }
    
    public Spatial getModel(){
        return model;
    }
    
    public void reset(){
        this.setLocalRotation(Quaternion.IDENTITY);
        this.setLocalTranslation(Vector3f.ZERO);
        model.setLocalRotation(Quaternion.IDENTITY);
        model.setLocalTranslation(Vector3f.ZERO);
    }
    
    public void setRotation(Quaternion rotation){
        this.setLocalRotation(rotation);
    }
    
    public void setOwner(long owner_id){
        this.owner_id = owner_id;
    }
    
    public abstract void action();
            
}
